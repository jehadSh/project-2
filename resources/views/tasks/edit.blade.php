@extends('layout.master')

@section('content')

<nav class="page-breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{route('tasks')}}">Tasks</a></li>
        <li class="breadcrumb-item"><a href="#">{{$task->title}}</a></li>
    </ol>
</nav>
<div class="row">
    <div class="col-lg-12 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">{{$task->title}}</h4>
                <form action="{{route('tasks.store')}}" method="post">
                    @csrf
                    <input type="hidden" name="id" value="{{$task->id}}">
                    <div class="form-group">
                        <label for="task-title" class="col-form-label">Title:</label>
                        <input type="text" name="title" class="form-control" id="task-title" value="{{$task->title}}" required>
                    </div>


                    <div class="form-group">
                         <label for="task-description" class="col-form-label">Description: <span style="color:red">*</span></label>
                         <input type="text" name="description" class="form-control" id="task-description" value="{{$task->description}}"required>
                     </div>

                    <div class="form-group">
                       <label for="task-hours" class="col-form-label">Hours: <span style="color:red">*</span></label>
                       <input type="number" name="hours" class="form-control" id="task-hours" value="{{$task->hours}}"required>
                    </div>
                     <div class="form-group">
                        <label for="task-min_grade" class="col-form-label">min_grade: <span style="color:red">*</span></label>
                        <input type="number" name="min_grade" class="form-control" id="task-min_grade" value="{{$task->min_grade}}"required>
                     </div>


                    <div class="table-responsive">
                        <table class="table table-striped">
                            <tbody>
                                <tr>
                                    <td>
                                        <button type="submit" class="btn btn-primary">Submit</button>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>


@endsection