@extends('layout.master')

@section('content')

<div class="d-flex justify-content-between align-items-center flex-wrap grid-margin">
    <div>
        <h4 class="mb-3 mb-md-0">Tasks</h4>
    </div>
    <div class="d-flex align-items-center flex-wrap text-nowrap">
      <button type="button" class="btn btn-primary btn-icon-text mb-2 mb-md-0" data-toggle="modal" data-target="#addTaskModal">
        <i class="btn-icon-prepend" data-feather="plus"></i>
         Add Task
      </button>
    </div>
</div>



<div class="row">
    <div class="col-lg-12 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">

                <div class="table-responsive">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Title</th>
                                <th>Description</th>

                            </tr>
                        </thead>
                        <tbody>
                            <div style="display:none;">{{$i=1}}</div>
                            @foreach($tasks as $task)
                            <tr>
                                <td>{{$i++}}</td>
                                <td>{{$task->title}}</td>
                                <td>{{$task->description}}</td>

                                <td>
                                <div class="dropdown mb-2">
                                  <button class="btn p-0" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i class="icon-lg text-muted pb-3px" data-feather="more-horizontal"></i>
                                  </button>
                                  <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                  <a class="dropdown-item d-flex align-items-center" href="{{route('tasks.edit',$task->id)}}"><i data-feather="edit-2" class="icon-sm mr-2"></i> <span class="">Edit</span></a>
                                    <a class="dropdown-item d-flex align-items-center" href="#" data-toggle="modal" data-target="#deleteTaskModal{{$task->id}}"><i data-feather="trash" class="icon-sm mr-2"></i> <span class="">Delete</span></a>
                                  </div>
                                 </div>
                                </td>
                            </tr>
                            @endforeach

                        </tbody>
                    </table>
                </div>

            </div>
        </div>
    </div>
</div>


<!-- Add Tasks Modal -->
<div class="modal fade" id="addTaskModal" tabindex="-1" role="dialog" aria-labelledby="addTaskModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="addTaskModalLabel">Add Task</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="{{route('tasks.store')}}" method="post">
        @csrf
        <div class="modal-body">
          <div class="form-group">
            <label for="task-title" class="col-form-label">Title: <span style="color:red">*</span></label>
            <input type="text" name="title" class="form-control" id="task-title" required>
          </div>
          <div class="form-group">
            <label for="task-description" class="col-form-label">Description: <span style="color:red">*</span></label>
            <input type="text" name="description" class="form-control" id="task-description" required>
          </div>
          <div class="form-group">
            <label for="task-hours" class="col-form-label">Hours: <span style="color:red">*</span></label>
            <input type="number" name="hours" class="form-control" id="task-hours" required>
          </div>
          <div class="form-group">
            <label for="task-min_grade" class="col-form-label">Min grade: <span style="color:red">*</span></label>
            <input type="number" name="min_grade" class="form-control" id="task-min_grade" required>
          </div>

          <div class="table-responsive">
             <label for="task-skills" class="col-form-label">Skills: <span style="color:red">*</span></label><br />
                 <table class="table table-striped">
                    <tbody>
                        @foreach($skills as $skill)
                           <tr>
                                <td>
                                   {{$skill->skillName}}
                                </td>
                                <td>
                                    <div class="form-check">
                                        <label class="form-check-label">
                                            <input type="checkbox" class="form-check-input" name="skill_Ids[]" value="{{$skill->id}}" >                                   
                                        </label>
                                     </div>
                                 </td>
                             </tr>
                        @endforeach
                   </tbody>
                </table>
           </div>
        </div>

        <div class="form-group">
            <label for="task-project_id" class="col-form-label">Project: <span style="color:red">*</span></label><br />
            @foreach($projects as $project)
            <div class="form-check form-check-inline">
              <label class="form-check-label">
                <input type="radio" class="form-check-input" name="project_id" id="optionsRadios6" value="{{$project->id}}" required>
                {{$project->title}}
              </label>
            </div>
            @endforeach
          </div>

          
           <div class="form-group">
            <label for="task-evaluation_date" class="col-form-label">Evaluation Date: <span style="color:red">*</span></label>
            <input type="date" name="evaluation_date" class="form-control" id="project-evaluation_date" required>
          </div>
        
        <div class="dropdown mb-2">
            <button class="btn p-0" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <label for="project-member" class="col-form-label">Members: <span style="color:red">*</span></label><br />
            </button>
            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                <div class="table-responsive">
                    <table class="table table-striped">
                        <tbody>
                            @foreach($profiles as $profile)
                                <tr>
                                    <td>
                                        {{$profile->userName}}
                                    </td>
                                    <td>
                                        <div class="form-check">
                                            <label class="form-check-label">
                                                <input type="checkbox" class="form-check-input" name="profile_Ids[]" value="{{$profile->id}}" >
                                               
                                            </label>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Add Task</button>
        </div>
      </form>
    </div>
  </div>
</div>


  <!-- Skill Delete Modal -->

  @foreach ($tasks as $task)
<div class="modal fade" id="deleteTaskModal{{$task->id}}" tabindex="-1" role="dialog" aria-labelledby="deleteTaskModal{{$task->id}}Label" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="deleteTaskModal{{$task->id}}Label">Are you sure you want to delete {{$task->title}}?</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-footer">
        <a type="button" class="btn btn-secondary" data-dismiss="modal">Close</a>
        <a href="{{route('tasks.destroy',$task->id)}}" class="btn btn-primary">Delete</a>
      </div>
    </div>
  </div>
</div>

@endforeach

@endsection
