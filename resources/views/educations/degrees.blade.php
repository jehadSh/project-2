@extends('layout.master')

@section('content')

<nav class="page-breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="#">Educations</a></li>
        <li class="breadcrumb-item active" aria-current="page">Degrees</li>
    </ol>
</nav>

<div class="row">
    <div class="col-md-6 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
                <h6 class="card-title">Published Degrees</h6>
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            <div style="display:none;">{{$i=1}}</div>
                            @foreach ($published_degrees as $pub)
                            <tr>
                                <td>{{$i++}}</td>
                                <td>{{$pub->degree}}</td>
                                <td>
                                    @can('update',$pub)
                                    <a href="{{route('educations.degree.publish',$pub->degree)}}" class="btn btn-primary">UnPublish</a>
                                    @endcan
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-6 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
                <h6 class="card-title">UnPublished Degrees</h6>
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            <div style="display:none;">{{$i=1}}</div>
                            @foreach ($unpublished_degrees as $unpub)
                            <tr>
                                <td>{{$i++}}</td>
                                <td>{{$unpub->degree}}</td>
                                <td>
                                    @can('update',$unpub)
                                    <a href="{{route('educations.degree.publish',$unpub->degree)}}" class="btn btn-primary">Publish</a>
                                    @endcan
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

</div>

@endsection