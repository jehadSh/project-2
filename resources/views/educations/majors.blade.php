@extends('layout.master')

@section('content')

<nav class="page-breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="#">Educations</a></li>
        <li class="breadcrumb-item active" aria-current="page">Majors</li>
    </ol>
</nav>

<div class="row">
    <div class="col-md-6 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
                <h6 class="card-title">Published Majors</h6>
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            <div style="display:none;">{{$i=1}}</div>
                            @foreach ($published_majors as $pub)
                            <tr>
                                <td>{{$i++}}</td>
                                <td>{{$pub->major}}</td>
                                <td>
                                    @can('update',$pub)
                                    <a href="{{route('educations.major.publish',$pub->major)}}" class="btn btn-primary">UnPublish</a>
                                    @endcan
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-6 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
                <h6 class="card-title">UnPublished Majors</h6>
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            <div style="display:none;">{{$i=1}}</div>
                            @foreach ($unpublished_majors as $unpub)
                            <tr>
                                <td>{{$i++}}</td>
                                <td>{{$unpub->major}}</td>
                                <td>
                                    @can('update',$unpub)
                                    <a href="{{route('educations.major.publish',$unpub->major)}}" class="btn btn-primary">Publish</a>
                                    @endcan
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

</div>

@endsection