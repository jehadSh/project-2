@extends('layout.master')

@section('content')

<nav class="page-breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{route('groups')}}">groups</a></li>
        <li class="breadcrumb-item"><a href="#">{{$project_id->project->title}} project</a></li>
    </ol>
</nav>



<div class="row">
    <div class="col-lg-12 grid-margin stretch-card">
        <div class="card">
            
            <div class="card-body">

                <h2 class="card-title">Project  {{$project_id->project->title}}</h4><br>
                <h5 class="card-title">Members:</h5>
                <div class="table-responsive">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                            </tr>
                        </thead>
                        <tbody>
                            <div style="display:none;">{{$i=1}}</div>
                            @foreach($profiles as $profile)
                            <tr>
                                <td>{{$i++}}</td>
                                <td>{{$profile->profile->userName}}</td>
                                <td>
                                <a class="btn btn-primary" href="#" data-toggle="modal" data-target="#deleteProfileModal{{$profile->Profile->userName}}"><i data-feather="trash" class="icon-sm mr-2"></i> <span class="">Delete</span></a>
                                </td>
                            </tr>
                            @endforeach

                        </tbody>
                    </table>
                </div>

            </div>
        </div>
    </div>
</div>










@foreach ($profiles as $profile)

<form action="{{route('groups.delete',$profile->Profile->userName)}}" method="post">
                    @csrf
<div class="modal fade" id="deleteProfileModal{{$profile->Profile->userName}}" tabindex="-1" role="dialog" aria-labelledby="deleteProfileModal{{$profile->Profile->userName}}Label" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
          
        <h5 class="modal-title" id="deleteProfileModal{{$profile->Profile->userName}}Label">Are you sure you want to delete {{$profile->profile->userName}}?</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <input type="hidden" name="project_id" value="{{$project_id->project->id}}">

      <div class="modal-footer">
        <a type="button" class="btn btn-secondary" data-dismiss="modal">Close</a>
        <button type="submit" class="btn btn-primary">Submit</button>

    </div>

    </div>
  </div>
</div>
</form>

@endforeach



@endsection
