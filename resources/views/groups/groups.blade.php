@extends('layout.master')

@section('content')

<div class="d-flex justify-content-between align-items-center flex-wrap grid-margin">
    <div>
        <h4 class="mb-3 mb-md-0">Groups</h4>
    </div>
</div>





<div class="row">
    <div class="col-lg-12 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">

                <div class="table-responsive">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Title</th>
                            </tr>
                        </thead>
                        <tbody>
                            <div style="display:none;">{{$i=1}}</div>
                            @foreach($groups as $group)
                            <tr>
                                <td>{{$i++}}</td>
                                <td>{{$group->project->title}}</td>
                                <td>
                                <div class="dropdown mb-2">
                                  <button class="btn p-0" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i class="icon-lg text-muted pb-3px" data-feather="more-horizontal"></i>
                                  </button>
                                  <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                    <a class="dropdown-item d-flex align-items-center" href="#" data-toggle="modal" data-target="#deleteGroupModal{{$group->project_id}}"><i data-feather="trash" class="icon-sm mr-2"></i> <span class="">Delete</span></a>
                                  </div>
                                 </div>
                                </td>
                                <td>
                                <a href="{{route('groups.show', $group->project_id)}}" class="btn btn-primary">Show Group</a>
                                </td>
                            </tr>
                            @endforeach

                        </tbody>
                    </table>
                </div>

            </div>
        </div>
    </div>
</div>






<!-- Group Delete Modal -->

@foreach ($groups as $group)
<div class="modal fade" id="deleteGroupModal{{$group->project_id}}" tabindex="-1" role="dialog" aria-labelledby="deleteGroupModal{{$group->project_id}}Label" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="deleteGroupModal{{$group->project_id}}Label">Are you sure you want to delete {{$group->project->title}}?</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-footer">
        <a type="button" class="btn btn-secondary" data-dismiss="modal">Close</a>
        <a href="{{route('groups.destroy',$group->project_id)}}" class="btn btn-primary">Delete</a>
      </div>
    </div>
  </div>
</div>

@endforeach


@endsection
