<nav class="navbar">
  <a href="#" class="sidebar-toggler">
    <i data-feather="menu"></i>
  </a>
  <div class="navbar-content">
    <form class="search-form" action="{{ route('profiles.search') }}" method="get">
      <div class="input-group">
        <div class="input-group-prepend">
          <div class="input-group-text">
            <i data-feather="search"></i>
          </div>
        </div>
        <input type="text" name="searchValue" class="form-control" placeholder="Search here...">
      </div>
    </form>
    <ul class="navbar-nav">

      <!-- <li class="nav-item dropdown nav-notifications">
        <a class="nav-link dropdown-toggle" href="#" id="notificationDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          <i data-feather="bell"></i> -->
          <!-- <div class="indicator"> -->
          <!-- <div class="circle"></div> -->
          <!-- </div> -->
        <!-- </a>
        <div class="dropdown-menu" aria-labelledby="notificationDropdown">
          <div class="dropdown-header d-flex align-items-center justify-content-between">
            <p class="mb-0 font-weight-medium">$NUM New Notifications</p>
          </div>
          <div class="dropdown-body">
          </div>
          <div class="dropdown-footer d-flex align-items-center justify-content-center">
            <a href="javascript:;">View all</a>
          </div>
        </div>
      </li> -->
      <li class="nav-item dropdown nav-profile">
        <a class="nav-link dropdown-toggle" href="#" id="profileDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          @if(auth()->user()->Profile->picture != null)
          <img src="{{ url(auth()->user()->Profile->picture) }}" size="30" alt="profile">
          @else
          <img src="{{ url('https://ui-avatars.com/api/?name='.auth()->user()->name) }}" alt="profile">
          @endif
        </a>
        <div class="dropdown-menu" aria-labelledby="profileDropdown">
          <div class="dropdown-header d-flex flex-column align-items-center">
            <div class="figure mb-3">
              @if(auth()->user()->Profile->picture != null)
              <img src="{{ url(auth()->user()->Profile->picture) }}" size="80">
              @else
              <img src="{{ url('https://ui-avatars.com/api/?name='.auth()->user()->name) }}">
              @endif
            </div>
            <div class="info text-center">
              <p class="name font-weight-bold mb-0">{{auth()->user()->name}}</p>
            </div>
          </div>
          <div class="dropdown-body">
            <ul class="profile-nav p-0 pt-3">
              <li class="nav-item">
                <a href="{{route('profiles.show',auth()->user()->Profile->id)}}" class="nav-link">
                  <i data-feather="user"></i>
                  {{ __('Profile') }}
                </a>
              </li>
              <!-- <li class="nav-item">
                <a href="javascript:;" class="nav-link">
                  <i data-feather="settings"></i>
                  <span>Account Settings</span>
                </a>
              </li> -->
              <li class="nav-item">
                <form method="POST" action="{{ route('logout') }}">
                  @csrf
                  <a class="nav-link" href="{{ route('logout') }}" onclick="event.preventDefault();this.closest('form').submit();">
                    <i data-feather="log-out"></i>
                    {{ __('Log Out') }}
                  </a>
                </form>
              </li>
            </ul>
          </div>
        </div>
      </li>
    </ul>
  </div>
</nav>