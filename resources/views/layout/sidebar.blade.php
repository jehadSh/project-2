<nav class="sidebar">
  <div class="sidebar-header">
    <a href="{{route('profiles')}}"class="sidebar-brand">
      <img src="{{ asset('assets/images/logo2.png') }}" alt="WorkSpace" style="width:130px;">
    </a>
    <div class="sidebar-toggler active">
      <span></span>
      <span></span>
      <span></span>
    </div>
  </div>
  <div class="sidebar-body">
    <ul class="nav">
      <li class="nav-item {{ active_class(['profiles']) }} {{ active_class(['profiles/*']) }}">
        <a class="nav-link" href="{{route('profiles')}}" role="button">
          <i class="link-icon" data-feather="user"></i>
          <span class="link-title">Profiles</span>
        </a>
      </li>
      <li class="nav-item {{ active_class(['roles']) }} {{ active_class(['roles/*']) }}">
        <a class="nav-link" href="{{route('roles')}}" role="button">
          <i class="link-icon" data-feather="slack"></i>
          <span class="link-title">Roles</span>
        </a>
      </li>
     {{-- <li class="nav-item {{ active_class(['educations/*']) }}">
        <a class="nav-link" data-toggle="collapse" href="#educations" role="button" aria-expanded="" aria-controls="educations">
          <i class="link-icon" data-feather="book"></i>
          <span class="link-title">Educations</span>
          <i class="link-arrow" data-feather="chevron-down"></i>
        </a>
        <div class="collapse " id="educations">
          <ul class="nav sub-menu">
            <li class="nav-item">
              <a href="{{ route('educations.degrees') }}" class="nav-link {{ active_class(['educations/degrees']) }}">Degrees</a>
            </li>
            <li class="nav-item">
              <a href="{{ route('educations.majors') }}" class="nav-link {{ active_class(['educations/majors']) }}">Majors</a>
            </li>
            <li class="nav-item">
              <a href="{{ route('educations.schools') }}" class="nav-link {{ active_class(['educations/schools']) }}">Schools</a>
            </li>
          </ul>
        </div>
      </li>--}}
      <li class="nav-item {{ active_class(['skills']) }} {{ active_class(['skills/*']) }}">
        <a class="nav-link" href="{{route('skills')}}" role="button">
          <i class="link-icon" data-feather="code"></i>
          <span class="link-title">Skills</span>
        </a>
      </li>
      <li class="nav-item {{ active_class(['commonSkills']) }} {{ active_class(['commonSkills/*']) }}">
        <a class="nav-link" href="{{route('commonSkills')}}" role="button">
          <i class="link-icon" data-feather="upload"></i>
          <span class="link-title">Common Skills</span>
        </a>
      </li>
      <li class="nav-item {{ active_class(['projects']) }} {{ active_class(['projects/*']) }}">
        <a class="nav-link" href="{{route('projects')}}" role="button">
          <i class="link-icon" data-feather="terminal"></i>
          <span class="link-title">Projects</span>
        </a>
      </li>
      <li class="nav-item {{ active_class(['tasks']) }} {{ active_class(['tasks/*']) }}">
        <a class="nav-link" href="{{route('tasks')}}" role="button">
          <i class="link-icon" data-feather="feather"></i>
          <span class="link-title">Tasks</span>
        </a>
      </li>
      <li class="nav-item {{ active_class(['groups']) }} {{ active_class(['groups/*']) }}">
        <a class="nav-link" href="{{route('groups')}}" role="button">
          <i class="link-icon" data-feather="user"></i>
          <span class="link-title">Groups</span>
        </a>
      </li>
    </ul>
  </div>
</nav>
