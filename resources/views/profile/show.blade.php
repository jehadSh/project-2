@extends('layout.master')

@section('content')
<div class="profile-page tx-13">
  <div class="row">
    <div class="col-12 grid-margin">
      <div class="profile-header">
        <div class="cover">
          <div class="gray-shade"></div>
          <figure>
            @if($profile->cover != null)
            <img src="{{ url($profile->cover) }}" style="max-height:272px" class="img-fluid" alt="profile cover">
            @else
            <img src="{{ url('https://via.placeholder.com/1148x272') }}" class="img-fluid" alt="profile cover">
            @endif
          </figure>
          <div class="cover-body d-flex justify-content-between align-items-center">
            <div>
              @if($profile->picture != null)
              <img class="profile-pic" src="{{ url($profile->picture) }}" style="max-height:100px" alt="profile">
              @else
              <img class="profile-pic" src="{{ url('https://ui-avatars.com/api/?name='.$profile->user->name) }}" alt="profile">
              @endif
              <span class="profile-name">{{$profile->user->name}}</span>
            </div>
            <div class="d-none d-md-block">
              @can('update',$profile)
              <button class="btn btn-primary btn-icon-text btn-edit-profile" data-toggle="modal" data-target="#accountSettingsModal">
                <i data-feather="edit" class="btn-icon-prepend"></i> Account settings
              </button>
              @endcan
            </div>
          </div>
        </div>
        <div class="header-links">
          <ul class="links d-flex align-items-center mt-3 mt-md-0">
            <li class="header-link-item d-flex align-items-center active">
              <i class="mr-1 icon-md" data-feather="user"></i>
              <a class="pt-1px d-none d-md-block" href="#">About</a>
            </li>
            <li class="header-link-item ml-3 pl-3 border-left d-flex align-items-center">
              <i class="mr-1 icon-md" data-feather="star"></i>
              <a class="pt-1px d-none d-md-block" href="#">Comments</a>
            </li>
            <li class="header-link-item ml-3 pl-3 border-left d-flex align-items-center">
              <i class="mr-1 icon-md" data-feather="file"></i>
              <a class="pt-1px d-none d-md-block" href="#">Files</a>
            </li>
          </ul>
        </div>
      </div>
    </div>
  </div>
  <div class="row profile-body">
    <!-- left wrapper start -->
    <div class="d-none d-md-block col-md-4 col-xl-4 left-wrapper">
      <div class="card rounded">
        <div class="card-body">
          <div class="d-flex align-items-center justify-content-between mb-2">
            <h6 class="card-title mb-0">About</h6>
            @if($profile->id == auth()->user()->Profile->id)
            @can('update',$profile)
            <a class="d-flex align-items-right text-secondary" style="cursor:pointer;"><i data-feather="edit-2" class="icon-sm mr-2"></i></a>
            @endcan
            @endif
          </div>
          <div class="mt-3">
            <label class="tx-11 font-weight-bold mb-0 text-uppercase">Weekly Hours:</label>
            <p class="text-muted">{{$profile->weekly_hours}}</p>
          </div>
          @if($profile->city != null)
          <div class="mt-3">
            <label class="tx-11 font-weight-bold mb-0 text-uppercase">Lives:</label>
            <p class="text-muted">{{$profile->city}}</p>
          </div>
          @endif
          @if($profile->gender != null)
          <div class="mt-3">
            <label class="tx-11 font-weight-bold mb-0 text-uppercase">Gender:</label>
            <p class="text-muted">{{$profile->gender}}</p>
          </div>
          @endif
          @if($profile->birthdate != null)
          <div class="mt-3">
            <label class="tx-11 font-weight-bold mb-0 text-uppercase">Birth Date:</label>
            <p class="text-muted">{{$profile->birthdate}}</p>
          </div>
          @endif
          <div class="mt-3">
            <label class="tx-11 font-weight-bold mb-0 text-uppercase">Joined:</label>
            <p class="text-muted">{{$profile->created_at}}</p>
          </div>
          @if($profile->contacts != null)
          <div class="mt-3">
            <label class="tx-11 font-weight-bold mb-0 text-uppercase">Contacts:</label>
            @foreach(json_decode($profile->contacts) as $contact)
            <p class="text-muted">{{$contact->name}}:&nbsp;{{$contact->value}}</p>
            @endforeach
          </div>
          @endif
          @if($profile->website != null)
          <div class="mt-3">
            <label class="tx-11 font-weight-bold mb-0 text-uppercase">Website:</label>
            <p class="text-muted">{{$profile->website}}</p>
          </div>
          @endif
          <div class="mt-3 d-flex social-links">
            @if($profile->github != null)
            <a href="{{$profile->github}}" class="btn d-flex align-items-center justify-content-center border mr-2 btn-icon github">
              <i data-feather="github" data-toggle="tooltip" title="{{$profile->github}}"></i>
            </a>
            @endif
            @if($profile->gitlab != null)
            <a href="{{$profile->gitlab}}" class="btn d-flex align-items-center justify-content-center border mr-2 btn-icon gitlab">
              <i data-feather="gitlab" data-toggle="tooltip" title="{{$profile->gitlab}}"></i>
            </a>
            @endif
          </div>
        </div>
      </div>
    </div>
    <!-- left wrapper end -->
    <!-- right wrapper start -->
    {{--<div class="col-md-8 col-xl-8 right-wrapper">
      <div class="row">
        <!-- card start -->
        <div class="col-md-12 grid-margin">
          <div class="card rounded">
            <div class="card-header">
              <div class="d-flex align-items-center justify-content-between">
                <div class="d-flex align-items-center">
                  <div class="ml-2">
                    <label class="tx-16 font-weight-bold mb-0 text-uppercase">Education</label>
                  </div>
                </div>
                @if($profile->id == auth()->user()->Profile->id)

                <a class="d-flex align-items-right text-secondary" style="cursor:pointer;" data-toggle="modal" data-target="#addEducationModal"><i data-feather="plus" class="icon-sm mr-2"></i></a>

                @endif
              </div>
            </div>
            <div class="card-body">

              @foreach ($educations as $education)
              <div class="d-flex align-items-center justify-content-between">
                <div style="cursor: pointer;" data-toggle="modal" data-target="#educationInfoModal{{$education->id}}">
                  <label class="mb-3 tx-14">{{$education->degree}} in {{$education->major}}, {{$education->school}}</label>
                </div>
                @if($profile->id == auth()->user()->Profile->id)
                @can('delete',$education)
                <a class="d-flex align-items-right text-secondary" style="cursor:pointer;" data-toggle="modal" data-target="#deleteEducationModal{{$education->id}}"><i data-feather="trash" class="icon-sm mr-2"></i></a>
                @endcan
                @endif
              </div>
              <p class="text-muted">
                Started: {{$education->start_date}}
                @if($education->end_date != null)
                Ended: {{$education->end_date}}
                @endif
              </p>
              <hr />
              @endforeach

            </div>
          </div>
        </div>
        <!-- card end -->
        <!-- card start -->
        <div class="col-md-12 grid-margin">
          <div class="card rounded">
            <div class="card-header">
              <div class="d-flex align-items-center justify-content-between">
                <div class="d-flex align-items-center">
                  <div class="ml-2">
                    <label class="tx-16 font-weight-bold mb-0 text-uppercase">Skills</label>
                  </div>
                </div>
                @if($profile->id == auth()->user()->Profile->id)

                <a class="d-flex align-items-right text-secondary" style="cursor:pointer;" data-toggle="modal" data-target="#addSkillModal"><i data-feather="plus" class="icon-sm mr-2"></i></a>

                @endif
              </div>
            </div>
            <div class="card-body">
              @foreach($skills as $skill)
              <div class="d-flex align-items-center justify-content-between">
                <div style="cursor: pointer;" data-toggle="modal" data-target="#skillInfoModal{{$skill->id}}">
                  <span class="pull-right">{{$skill->name}}</span>
                </div>
                @can('update',$skill)
                <a class="d-flex align-items-right text-secondary" style="cursor:pointer;" data-toggle="modal" data-target="#skillEvaluateModal{{$skill->id}}"><i data-feather="star" class="icon-sm mr-2"></i></a>
                @endcan
                @if($profile->id == auth()->user()->Profile->id)
                @can('delete',$skill)
                <a class="d-flex align-items-right text-secondary" style="cursor:pointer;" data-toggle="modal" data-target="#skillDeleteModal{{$skill->id}}"><i data-feather="trash" class="icon-sm mr-2"></i></a>
                @endcan
                @endif
              </div>
              <div style="cursor: pointer;" data-toggle="modal" data-target="#skillInfoModal{{$skill->id}}">
                <div class="progress">
                  @if($skill->evaluation!=null)
                  <div class="progress-bar bg-primary" role="progressbar" style="width: {{$skill->evaluation}}0%;">{{$skill->evaluation}}/10</div>
                  @endif
                </div>
                <span class="pull-right">{{$skill->last_use}}</span>
              </div>
              <br /><br />
              @endforeach
            </div>
          </div>
        </div>
        <!-- card end -->
      </div>
    </div>--}}
    <!-- right wrapper end -->
  </div>
</div>

<!-- Account settings Modal -->
@can('update',$profile)
<div class="modal fade" id="accountSettingsModal" tabindex="-1" role="dialog" aria-labelledby="accountSettingsModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="accountSettingsModalLabel">Account settings</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="{{route('accounts.update',$profile->id)}}" method="post">
        @csrf
        <div class="modal-body">
          <div class="form-group">
            <label for="education-degree" class="col-form-label">User Status:</label><br />
            @foreach($userStatus as $aStatus)
            <div class="form-check form-check-inline">
              <label class="form-check-label">
                @if($profile->user->status==$aStatus)
                <input type="radio" class="form-check-input" name="user_status" value="{{$aStatus}}" checked>
                @else
                <input type="radio" class="form-check-input" name="user_status" value="{{$aStatus}}">
                @endif
                {{$aStatus}}
              </label>
            </div>
            @endforeach
          </div>
          <div class="form-group">
            <label for="education-degree" class="col-form-label">Profile Status:</label><br />
            @foreach($profileStatus as $aStatus)
            <div class="form-check form-check-inline">
              <label class="form-check-label">
                @if($profile->status==$aStatus)
                <input type="radio" class="form-check-input" name="profile_status" value="{{$aStatus}}" checked>
                @else
                <input type="radio" class="form-check-input" name="profile_status" value="{{$aStatus}}">
                @endif
                {{$aStatus}}
              </label>
            </div>
            @endforeach
          </div>
          <div class="form-group">
            <label for="education-degree" class="col-form-label">Roles:</label><br />
            @foreach($roles as $role)
            <div class="form-check">
              <label class="form-check-label">
                @if(in_array($role->id,$userRoles))
                <input type="checkbox" class="form-check-input" name="roles[]" value="{{$role->id}}" checked>
                @else
                <input type="checkbox" class="form-check-input" name="roles[]" value="{{$role->id}}">
                @endif
                {{$role->name}}
              </label>
            </div>
            @endforeach
          </div>
        </div>
        <div class="modal-footer">
          <a type="button" class="btn btn-secondary" data-dismiss="modal">Close</a>
          <button type="submit" class="btn btn-primary">Edit</button>
        </div>
      </form>
    </div>
  </div>
</div>
@endcan


<!-- Add Education Modal -->
@if($profile->id == auth()->user()->Profile->id)

<div class="modal fade" id="addEducationModal" tabindex="-1" role="dialog" aria-labelledby="addEducationModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="addEducationModalLabel">Add Education</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="{{route('educations.store')}}" method="post">
        @csrf
        <div class="modal-body">
          <div class="form-group">
            <label for="education-degree" class="col-form-label">Degree: <span style="color:red">*</span></label>
            <input type="text" name="degree" class="form-control" id="education-degree" required>
          </div>
          <div class="form-group">
            <label for="education-major" class="col-form-label">Major: <span style="color:red">*</span></label>
            <input type="text" name="major" class="form-control" id="education-major" required>
          </div>
          <div class="form-group">
            <label for="education-school" class="col-form-label">School: <span style="color:red">*</span></label>
            <input type="text" name="school" class="form-control" id="education-school" required>
          </div>
          <div class="form-group">
            <label for="education-activities" class="col-form-label">Activities:</label>
            <textarea name="activities" rows="5" class="form-control" id="education-activities"></textarea>
          </div>
          <div class="form-group">
            <label for="education-startDate" class="col-form-label">Start date: <span style="color:red">*</span></label>
            <input type="date" name="start_date" class="form-control" id="education-startDate" required placeholder>
          </div>
          <div class="form-group">
            <label for="education-endDate" class="col-form-label">End date:</label>
            <input type="date" name="end_date" class="form-control" id="education-endDate">
          </div>
        </div>
        <div class="modal-footer">
          <a type="button" class="btn btn-secondary" data-dismiss="modal">Close</a>
          <button type="submit" class="btn btn-primary">Add Education</button>
        </div>
      </form>
    </div>
  </div>
</div>

@endif



<!-- Education Info Modal -->
{{--@foreach ($educations as $education)
<div class="modal fade" id="educationInfoModal{{$education->id}}" tabindex="-1" role="dialog" aria-labelledby="educationInfoModal{{$education->id}}Label" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="educationInfoModal{{$education->id}}Label">{{$education->degree}} in {{$education->major}}, {{$education->school}}</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        @if($education->activities!=null)
        <label class="tx-11 font-weight-bold mb-0 text-uppercase">Activities:</label>
        <p class="text-muted">{{$education->activities}}</p><br />
        @endif
        <label class="tx-11 font-weight-bold mb-0 text-uppercase">Start date:</label>
        <p class="text-muted">{{$education->start_date}}</p><br />
        @if($education->end_date!=null)
        <label class="tx-11 font-weight-bold mb-0 text-uppercase">End date:</label>
        <p class="text-muted">{{$education->end_date}}</p>
        @endif
      </div>
      <div class="modal-footer">
        <a type="button" class="btn btn-secondary" data-dismiss="modal">Close</a>
        @if($profile->id != auth()->user()->Profile->id)
        <!-- //FIXME: Complete-->
        <button type="submit" href="{{route('educations.store')}}" class="btn btn-primary">Edit</button>
        @endif
      </div>
    </div>
  </div>
</div>
@endforeach--}}


<!-- Delete Education Modal -->
@if($profile->id == auth()->user()->Profile->id)
{{--@foreach ($educations as $education)
@can('delete',$education)
<div class="modal fade" id="deleteEducationModal{{$education->id}}" tabindex="-1" role="dialog" aria-labelledby="deleteEducationModal{{$education->id}}Label" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="deleteEducationModal{{$education->id}}Label">Are you sure you want to delete this education?</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-footer">
        <a type="button" class="btn btn-secondary" data-dismiss="modal">Close</a>
        <a href="{{route('educations.destroy',$education->id)}}" class="btn btn-primary">Delete</a>
      </div>
    </div>
  </div>
</div>
@endcan
@endforeach--}}
@endif


<!-- Add Skill Modal -->
@if($profile->id == auth()->user()->Profile->id)

<div class="modal fade" id="addSkillModal" tabindex="-1" role="dialog" aria-labelledby="addSkillModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="addSkillModalLabel">Add Skill</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="{{route('skills.store')}}" method="post">
        @csrf
        <div class="modal-body">
          <div class="form-group">
            <label for="skill-name" class="col-form-label">Name: <span style="color:red">*</span></label>
            <input type="text" name="name" class="form-control" id="skill-name" required>
          </div>
          <div class="form-group">
            <label for="skill-level" class="col-form-label">Level: <span style="color:red">*</span></label>
            <input type="number" name="level" class="form-control" id="skill-level" min="1" max="10">
          </div>
          <div class="form-group">
            <label for="skill-description" class="col-form-label">Description:</label>
            <textarea name="description" rows="5" class="form-control" id="skill-description"></textarea>
          </div>
          <div class="form-group">
            <label for="skill-lastuse" class="col-form-label">Last Use: <span style="color:red">*</span></label>
            <input type="date" name="last_use" class="form-control" id="skill-lastuse" max="{{ now()->toDateString('Y-m-d') }}" required>
          </div>
        </div>
        <div class="modal-footer">
          <a type="button" class="btn btn-secondary" data-dismiss="modal">Close</a>
          <button type="submit" class="btn btn-primary">Add Skill</button>
        </div>
      </form>
    </div>
  </div>
</div>

@endif



<!-- Skill Info Modal -->
@foreach($skills as $skill)
<div class="modal fade" id="skillInfoModal{{$skill->id}}" tabindex="-1" role="dialog" aria-labelledby="skillInfoModal{{$skill->id}}Label" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="skillInfoModal{{$skill->id}}Label">{{$skill->name}}</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="{{route('skills.store')}}" method="post">
        @csrf
        <input type="hidden" name="id" value="{{$skill->id}}">
        <div class="modal-body">

          @if($profile->id == auth()->user()->Profile->id)
          <label class="tx-11 font-weight-bold mb-0 text-uppercase">Description:</label><br />
          <textarea name="description" rows="5" class="form-control" id="skill-description">{{$skill->description}}</textarea>
          <label class="tx-11 font-weight-bold mb-0 text-uppercase">Last Use:</label><br />
          <input type="date" name="last_use" class="form-control" id="skill-lastuse" max="{{ now()->toDateString('Y-m-d') }}" value="{{$skill->last_use}}">

          @else

          @if($skill->description != null)
          <label class="tx-11 font-weight-bold mb-0 text-uppercase">Description:</label><br />
          <p class="text-muted">{{$skill->description}}</p>
          @endif
          @if($skill->last_use != null)
          <label class="tx-11 font-weight-bold mb-0 text-uppercase">Last Use:</label><br />
          <p class="text-muted">{{$skill->last_use}}</p>
          @endif

          @endif
        </div>
        <div class="modal-footer">
          <a type="button" class="btn btn-secondary" data-dismiss="modal">Close</a>
          @if($profile->id == auth()->user()->Profile->id)
          @can('update',$skill)
          <button type="submit" class="btn btn-primary">Edit</button>
          @endcan
          @endif
        </div>
      </form>
    </div>
  </div>
</div>
@endforeach


<!-- Skill Evaluate Modal -->
@foreach($skills as $skill)
@can('update',$skill)
<div class="modal fade" id="skillEvaluateModal{{$skill->id}}" tabindex="-1" role="dialog" aria-labelledby="skillEvaluateModal{{$skill->id}}Label" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="skillEvaluateModal{{$skill->id}}Label">Evaluate {{$skill->name}}</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="{{route('skills.evaluate')}}" method="post">
        @csrf
        <div class="modal-body">
          <input type="hidden" name="id" value="{{$skill->id}}">
          <label for="skill-evaluation" class="col-form-label">Value:</label>
          <input type="number" name="evaluation" class="form-control" id="skill-evaluation" min="0" max="10">
        </div>
        <div class="modal-footer">
          <a type="button" class="btn btn-secondary" data-dismiss="modal">Close</a>
          <button type="submit" href="{{route('skills.evaluate',$skill->id)}}" class="btn btn-primary">Evaluate</button>
        </div>
      </form>
    </div>
  </div>
</div>
@endcan
@endforeach


<!-- Skill Delete Modal -->
@if($profile->id == auth()->user()->Profile->id)
@foreach($skills as $skill)
@can('delete',$skill)
<div class="modal fade" id="skillDeleteModal{{$skill->id}}" tabindex="-1" role="dialog" aria-labelledby="skillDeleteModal{{$skill->id}}Label" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="skillDeleteModal{{$skill->id}}Label">Are you sure you want to delete your {{$skill->name}} skill?</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-footer">
        <a type="button" class="btn btn-secondary" data-dismiss="modal">Close</a>
        <a href="{{route('skills.destroy',$skill->id)}}" class="btn btn-primary">Delete</a>
      </div>
    </div>
  </div>
</div>
@endcan
@endforeach
@endif


@endsection
