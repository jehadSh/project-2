@extends('layout.master')

@section('content')

<div class="d-flex justify-content-between align-items-center flex-wrap grid-margin">
    <div>
        <h4 class="mb-3 mb-md-0">Profiles</h4>
    </div>
</div>

<div class="row">
    @foreach ($profiles as $profile)
    @can('view-any',$profile)
    <div class="col-12 col-md-6 col-xl-4 grid-margin grid-margin-md-5">
        <div class="card">
            @if($profile->picture != null)
            <img class="card-img-top stretched-img" src="{{ url($profile->picture) }}" style="height:100px">
            @else
            <img class="card-img-top stretched-img" src="{{ url('https://ui-avatars.com/api/?name='.$profile->user->name) }}" style="height:100px">
            @endif
            <div class="card-body">
                <h5 class="card-title">{{$profile->user->name}}</h5>
                <h6 class="card-text">Roles</h6>
                @foreach ($profile->user->roles as $role)
                <p class="card-text">{{$role->name}}</p>
                @endforeach
                <br/>
                <a href="{{route('profiles.show', $profile->id)}}" class="btn btn-primary">Show Profile</a>
            </div>
        </div>
    </div>
    @endcan
    @endforeach
</div>

<div class="d-flex justify-content-center">
    {!! $profiles->links() !!}
</div>

@endsection