@extends('layout.master')

@section('content')

<div class="d-flex justify-content-between align-items-center flex-wrap grid-margin">
    <div>
        <h4 class="mb-3 mb-md-0">Common skills</h4>
    </div>
    <div class="d-flex align-items-center flex-wrap text-nowrap">
    <button type="button" class="btn btn-primary btn-icon-text mb-2 mb-md-0" data-toggle="modal" data-target="#addCommonSkillModal">
      <i class="btn-icon-prepend" data-feather="plus"></i>
      Add Common Skill
    </button>
  </div>
</div>



<div class="row">
<div class="col-lg-12 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
                <h6 class="card-title">Common Skills</h6>
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            <div style="display:none;">{{$i=1}}</div>
                            @foreach ($commonSkills as $skill)
                            <tr>
                                <td>{{$i++}}</td>
                                <td>{{$skill->skillName}}</td>
                                <td>
                                <div class="dropdown mb-2">
                                     <button class="btn p-0" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                       <i class="icon-lg text-muted pb-3px" data-feather="more-horizontal"></i>
                                    </button>
                                      <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                      <a class="dropdown-item d-flex align-items-center" href="#" data-toggle="modal" data-target="#editCommonSkillModal{{$skill->id}}"><i data-feather="edit-2" class="icon-sm mr-2"></i> <span class="">Edit</span></a>
                                      <a class="dropdown-item d-flex align-items-center" href="#" data-toggle="modal" data-target="#deleteCommonSkillModal{{$skill->skillName}}"><i data-feather="trash" class="icon-sm mr-2"></i> <span class="">Delete</span></a>
                                     </div>
                                    </div>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

</div>




<!-- Add CommonSkill Modal -->
<div class="modal fade" id="addCommonSkillModal" tabindex="-1" role="dialog" aria-labelledby="addCommonSkillModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="addCommonSkillModalLabel">Add Common Skill</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="{{route('commonSkills.store')}}" method="post">
        @csrf
        <div class="modal-body">
          <div class="form-group">
            <label for="Commonskill-name" class="col-form-label">Name: <span style="color:red">*</span></label>
            <input type="text" name="name" class="form-control" id="Commonskill-name" required>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Add Common Skill</button>
        </div>
      </form>
    </div>
  </div>
</div>



<!-- CommonSkill Delete Modal -->

@foreach ($commonSkills as $skill)
<div class="modal fade" id="deleteCommonSkillModal{{$skill->skillName}}" tabindex="-1" role="dialog" aria-labelledby="deleteCommonSkillModal{{$skill->skillName}}Label" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="deleteCommonSkillModal{{$skill->skillName}}Label">Are you sure you want to delete {{$skill->skillName}}?</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-footer">
        <a type="button" class="btn btn-secondary" data-dismiss="modal">Close</a>
        <a href="{{route('commonSkills.destroy',$skill->skillName)}}" class="btn btn-primary">Delete</a>
      </div>
    </div>
  </div>
</div>

@endforeach


<!-- CommonSkill Edit Modal -->

@foreach ($commonSkills as $skill)
<div class="modal fade" id="editCommonSkillModal{{$skill->id}}" tabindex="-1" role="dialog" aria-labelledby="editCommonSkillModal{{$skill->id}}Label" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="editCommonSkillModal{{$skill->id}}Label">Edit Common Skill {{$skill->skillName}}?</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="{{route('commonSkills.update',$skill->id)}}" method="post">
        @csrf
        <div class="modal-body">
          <div class="form-group">
          <input type="hidden" name="id" value="{{$skill->id}}">
            <label for="Commonskill-name" class="col-form-label">Name: <span style="color:red">*</span></label>
            <input type="text" name="name" class="form-control" id="Commonskill-name" value="{{$skill->skillName}}" required>
          </div>
        </div>
      <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Edit</button>
      </div>
    </form>
    </div>
  </div>
</div>

@endforeach


@endsection
