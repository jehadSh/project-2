@extends('layout.master')

@section('content')

<nav class="page-breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{route('projects')}}">Projects</a></li>
        <li class="breadcrumb-item"><a href="#">{{$project->title}}</a></li>
    </ol>
</nav>

<div class="row">
    <div class="col-lg-12 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">{{$project->title}}</h4>
                <form action="{{route('projects.store')}}" method="post">
                    @csrf
                    <input type="hidden" name="id" value="{{$project->id}}">
                    <div class="form-group">
                        <label for="project-title" class="col-form-label">Title:</label>
                        <input type="text" name="title" class="form-control" id="project-title" value="{{$project->title}}" required>
                    </div>
                    <div class="form-group">
                         <label for="project-description" class="col-form-label">Description: <span style="color:red">*</span></label>
                         <input type="text" name="description" class="form-control" id="project-description" value="{{$project->description}}"required>
                     </div>
                     <div class="form-group">
                        <label for="project-start_date" class="col-form-label">Start Date: <span style="color:red">*</span></label>
                        <input type="date" name="start_date" class="form-control" id="project-start_date" value="{{$project->start_date}}"required>
                     </div>
                     <div class="form-group">
                        <label for="project-estimated_date" class="col-form-label">Estimated Date: <span style="color:red">*</span></label>
                        <input type="date" name="estimated_date" class="form-control" id="project-estimated_date" value="{{$project->estimated_date}}"required>
                     </div>

                     <div class="form-group">
                        <label for="project-status" class="col-form-label">Status:</label><br />

                        @foreach($status as $aStatus)
                        <div class="form-check form-check-inline">
                            <label class="form-check-label">
                                @if($project->status==$aStatus)
                                <input type="radio" class="form-check-input" name="status" id="optionsRadios6" value="{{$aStatus}}" checked>
                                @else
                                <input type="radio" class="form-check-input" name="status" id="optionsRadios6" value="{{$aStatus}}">
                                @endif
                                {{$aStatus}}
                            </label>
                        </div>
                        @endforeach

          


                    <div class="table-responsive">
                        <table class="table table-striped">
                            <tbody>
                                <tr>
                                    <td>
                                        <button type="submit" class="btn btn-primary">Submit</button>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
    
                </form>
            </div>
        </div>
    </div>
</div>



@endsection