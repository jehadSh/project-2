@extends('layout.master')

@section('content')

<div class="d-flex justify-content-between align-items-center flex-wrap grid-margin">
    <div>
        <h4 class="mb-3 mb-md-0">Projects</h4>
    </div>
    <div class="d-flex align-items-center flex-wrap text-nowrap">
    <button type="button" class="btn btn-primary btn-icon-text mb-2 mb-md-0" data-toggle="modal" data-target="#addProjectModal">
      <i class="btn-icon-prepend" data-feather="plus"></i>
      Add Project
    </button>
  </div>
</div>

<div class="row">
    <div class="col-lg-12 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">

                <div class="table-responsive">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Title</th>
                                <th>Status</th>
                            </tr>
                        </thead>
                        <tbody>
                            <div style="display:none;">{{$i=1}}</div>
                            @foreach($projects as $project)
                            <tr>
                                <td>{{$i++}}</td>
                                <td>{{$project->title}}</td>
                                <td>
                                    <a style="cursor: pointer;" data-toggle="modal" data-target="#updateProject{{$project->id}}Modal">{{$project->status}}</a>
                                </td>
                                <td>
                                <div class="dropdown mb-2">
                                  <button class="btn p-0" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i class="icon-lg text-muted pb-3px" data-feather="more-horizontal"></i>
                                  </button>
                                  <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                  <a class="dropdown-item d-flex align-items-center" href="{{route('projects.edit',$project->id)}}"><i data-feather="edit-2" class="icon-sm mr-2"></i> <span class="">Edit</span></a>
                                    <a class="dropdown-item d-flex align-items-center" href="#" data-toggle="modal" data-target="#deleteProjectModal{{$project->id}}"><i data-feather="trash" class="icon-sm mr-2"></i> <span class="">Delete</span></a>
                                  </div>
                                 </div>
                                </td>
                            </tr>
                            @endforeach

                        </tbody>
                    </table>
                </div>

            </div>
        </div>
    </div>
</div>








<!-- Add Project Modal -->
<div class="modal fade" id="addProjectModal" tabindex="-1" role="dialog" aria-labelledby="addProjectModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="addPrjectModalLabel">Add Project</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="{{route('projects.store')}}" method="post">
        @csrf
        <div class="modal-body">
          <div class="form-group">
            <label for="project-title" class="col-form-label">Title: <span style="color:red">*</span></label>
            <input type="text" name="title" class="form-control" id="project-title" required>
          </div>
          <div class="form-group">
            <label for="project-description" class="col-form-label">Description: <span style="color:red">*</span></label>
            <input type="text" name="description" class="form-control" id="project-description" required>
          </div>
          <div class="form-group">
            <label for="project-start_date" class="col-form-label">Starting Date: <span style="color:red">*</span></label>
            <input type="date" name="start_date" class="form-control" id="project-start_date" required>
          </div>
          <div class="form-group">
            <label for="project-estimated_date" class="col-form-label">Estimated Date: <span style="color:red">*</span></label>
            <input type="date" name="estimated_date" class="form-control" id="project-estimated_date" required>
          </div>

          <div class="form-group">
            <label for="project-manager_id" class="col-form-label">Manager: <span style="color:red">*</span></label><br>
            @foreach($managers as $manager)
            <div class="form-check form-check-inline">
              <label class="form-check-label">
                <input type="radio" class="form-check-input" name="manager_id" id="optionsRadios6" value="{{$manager->id}}" required>
                {{$manager->name}}
              </label>
            </div>
            @endforeach         
           </div>



           <div class="form-group">
            <label for="project-isPublic" class="col-form-label">Is Public: <span style="color:red">*</span></label><br />
            <div class="form-check form-check-inline">
              <label class="form-check-label">
                <input type="radio" class="form-check-input" name="isPublic" id="optionsRadios6" value="1" required>
                yes
              </label>
            </div>
            <div class="form-check form-check-inline">
              <label class="form-check-label">
                <input type="radio" class="form-check-input" name="isPublic" id="optionsRadios6" value="0" required>
                No
              </label>
            </div>
          </div>
          <div class="form-group">
            <label for="project-status" class="col-form-label">Status: <span style="color:red">*</span></label><br />
            @foreach($status as $aStatus)
            <div class="form-check form-check-inline">
              <label class="form-check-label">
                <input type="radio" class="form-check-input" name="status" id="optionsRadios6" value="{{$aStatus}}" required>
                {{$aStatus}}
              </label>
            </div>
            @endforeach
          </div>



          <div class="table-responsive">
          <label for="project-skills" class="col-form-label">Skills: <span style="color:red">*</span></label><br />
                        <table class="table table-striped">
                            <tbody>
                                @foreach($skills as $skill)
                                <tr>
                                    <td>
                                        {{$skill->skillName}}
                                    </td>
                                    <td>
                                        <div class="form-check">
                                            <label class="form-check-label">
                                                <input type="checkbox" class="form-check-input" name="skills[]" value="{{$skill->skillName}}" >
                                               
                                            </label>
                                        </div>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>


                    <div class="dropdown mb-2">
                        <button class="btn p-0" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <label for="project-member" class="col-form-label">Members: <span style="color:red">*</span></label><br />
                        </button>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                        <div class="table-responsive">
                        <table class="table table-striped">
                            <tbody>
                                @foreach($profiles as $profile)
                                <tr>
                                    <td>
                                        {{$profile->userName}}
                                    </td>
                                    <td>
                                        <div class="form-check">
                                            <label class="form-check-label">
                                                <input type="checkbox" class="form-check-input" name="profile_Ids[]" value="{{$profile->id}}" >
                                               
                                            </label>
                                        </div>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
    
                    </div>
                    </div>


                    

          
          <div class="form-group">
            <label for="project-color" class="col-form-label">Color: <span style="color:red">*</span></label><br />
            <div class="form-check form-check-inline">
              <label class="form-check-label">
                <input type="radio" class="form-check-input" name="color" id="optionsRadios6" value="red" required>
                Red
              </label>
            </div>
            <div class="form-check form-check-inline">
              <label class="form-check-label">
                <input type="radio" class="form-check-input" name="color" id="optionsRadios6" value="blue" required>
                Blue
              </label>
              
            </div>
            <div class="form-check form-check-inline">
              <label class="form-check-label">
                <input type="radio" class="form-check-input" name="color" id="optionsRadios6" value="black" required>
                Black
              </label>
              
            </div>
            <div class="form-check form-check-inline">
              <label class="form-check-label">
                <input type="radio" class="form-check-input" name="color" id="optionsRadios6" value="White" required>
                White
              </label>
            </div>
          </div>
          
        </div>  
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Add Project</button>
        </div>
      </form>
    </div>
    </div>
</div>



<div class="d-flex justify-content-center">
    {!! $projects->links() !!}
</div>

@foreach($projects as $project)
@can('update',$project)
<div class="modal fade" id="updateProject{{$project->id}}Modal" tabindex="-1" role="dialog" aria-labelledby="updateProject{{$project->id}}ModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="updateProject{{$project->id}}ModalLabel">Project Status</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="{{route('projects.update',$project->id)}}" method="post">
                @csrf
                <div class="modal-body">
                    <div class="form-group">
                        @foreach($status as $aStatus)
                        <div class="form-check form-check-inline">
                            <label class="form-check-label">
                                @if($project->status==$aStatus)
                                <input type="radio" class="form-check-input" name="status" value="{{$aStatus}}" checked>
                                @else
                                <input type="radio" class="form-check-input" name="status" value="{{$aStatus}}">
                                @endif
                                {{$aStatus}}
                            </label>
                        </div>
                        @endforeach
                    </div>
                </div>
                <div class="modal-footer">
                    <a type="button" class="btn btn-secondary" data-dismiss="modal">Close</a>
                    <button type="submit" class="btn btn-primary">Edit</button>
                </div>
            </form>
        </div>
    </div>
</div>

@endcan
@endforeach


<!-- Project Delete Modal -->

@foreach ($projects as $project)
<div class="modal fade" id="deleteProjectModal{{$project->id}}" tabindex="-1" role="dialog" aria-labelledby="deleteProjectModal{{$project->id}}Label" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="deleteProjectModal{{$project->id}}Label">Are you sure you want to delete {{$project->title}}?</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-footer">
        <a type="button" class="btn btn-secondary" data-dismiss="modal">Close</a>
        <a href="{{route('projects.destroy',$project->id)}}" class="btn btn-primary">Delete</a>
      </div>
    </div>
  </div>
</div>

@endforeach

@endsection
