@extends('layout.master')

@section('content')

<nav class="page-breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{route('roles')}}">Roles</a></li>
        <li class="breadcrumb-item"><a href="#">{{$role->name}}</a></li>
        <li class="breadcrumb-item active" aria-current="page">Permissions</li>
    </ol>
</nav>

<div class="row">
    <div class="col-lg-12 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">{{$role->name}}</h4>
                <form action="{{route('roles.store')}}" method="post">
                    @csrf
                    <input type="hidden" name="id" value="{{$role->id}}">
                    <div class="form-group">
                        <label for="role-name" class="col-form-label">Name:</label>
                        <input type="text" name="name" class="form-control" id="role-name" value="{{$role->name}}" required>
                    </div>
                    <div class="form-group">
                        <label for="role-status" class="col-form-label">Status:</label><br />

                        @foreach($status as $aStatus)
                        <div class="form-check form-check-inline">
                            <label class="form-check-label">
                                @if($role->status==$aStatus)
                                <input type="radio" class="form-check-input" name="status" id="optionsRadios6" value="{{$aStatus}}" checked>
                                @else
                                <input type="radio" class="form-check-input" name="status" id="optionsRadios6" value="{{$aStatus}}">
                                @endif
                                {{$aStatus}}
                            </label>
                        </div>
                        @endforeach

                    </div>

                    <div class="table-responsive">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>
                                        Permission
                                    </th>
                                    @foreach($models as $model)
                                    <th>
                                        {{$model}}
                                    </th>
                                    @endforeach
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($permissions as $permission)
                                <tr>
                                    <td>
                                        {{$permission}}
                                    </td>
                                    @foreach($models as $model)
                                    <td>
                                        <div class="form-check">
                                            <label class="form-check-label">
                                                @if(in_array("$permission$model",$rolePermissions))
                                                <input type="checkbox" class="form-check-input" name="permissions[]" value="{{$permission}}{{$model}}" checked>
                                                @else
                                                <input type="checkbox" class="form-check-input" name="permissions[]" value="{{$permission}}{{$model}}">
                                                @endif
                                            </label>
                                        </div>
                                    </td>
                                    @endforeach
                                </tr>
                                @endforeach
                                <tr>
                                    @foreach($models as $model)
                                    <td>
                                    </td>
                                    @endforeach
                                    <td>
                                        <button type="submit" class="btn btn-primary">Submit</button>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>



@endsection