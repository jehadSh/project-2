@extends('layout.master')

@section('content')

<div class="d-flex justify-content-between align-items-center flex-wrap grid-margin">
  <div>
    <h4 class="mb-3 mb-md-0">Roles</h4>
  </div>
  <div class="d-flex align-items-center flex-wrap text-nowrap">
    <button type="button" class="btn btn-primary btn-icon-text mb-2 mb-md-0" data-toggle="modal" data-target="#addRoleModal">
      <i class="btn-icon-prepend" data-feather="plus"></i>
      Add Role
    </button>
  </div>
</div>

<div class="row">
  <div class="col-12 col-xl-12 stretch-card">
    <div class="row flex-grow">

      @foreach ($roles as $role)

      <div class="col-md-4 grid-margin stretch-card">
        <div class="card">
          <div class="card-body">
            <div class="d-flex justify-content-between align-items-baseline">
              <h3 class="card-title mb-0">{{$role->name}}</h3>
              <div class="dropdown mb-2">
                <button class="btn p-0" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <i class="icon-lg text-muted pb-3px" data-feather="more-horizontal"></i>
                </button>
                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                  <a class="dropdown-item d-flex align-items-center" href="{{route('roles.edit',$role->id)}}"><i data-feather="edit-2" class="icon-sm mr-2"></i> <span class="">Edit</span></a>
                  <a class="dropdown-item d-flex align-items-center" href="#" data-toggle="modal" data-target="#deleteRoleModal{{$role->id}}"><i data-feather="trash" class="icon-sm mr-2"></i> <span class="">Delete</span></a>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-6 col-md-12 col-xl-5">
                <h6 class="mb-2 text-info">{{$role->status}}</h6>
                <div class="d-flex align-items-baseline">
                  <p class="text-info">
                    <!-- <span>users count</span> -->
                  </p>
                </div>
              </div>
              <div class="col-6 col-md-12 col-xl-7">
                <div id="apexChart1" class="mt-md-3 mt-xl-0"></div>
              </div>
            </div>
          </div>
        </div>
      </div>

      @endforeach

    </div>
  </div>
</div>

<!-- Add Role Modal -->
<div class="modal fade" id="addRoleModal" tabindex="-1" role="dialog" aria-labelledby="addRoleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="addRoleModalLabel">Add Role</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="{{route('roles.store')}}" method="post">
        @csrf
        <div class="modal-body">
          <div class="form-group">
            <label for="role-name" class="col-form-label">Name: <span style="color:red">*</span></label>
            <input type="text" name="name" class="form-control" id="role-name" required>
          </div>
          <div class="form-group">
            <label for="role-alias" class="col-form-label">Alias: <span style="color:red">*</span></label>
            <input type="text" name="alias" class="form-control" id="role-alias" required>
          </div>
          <div class="form-group">
            <label for="role-status" class="col-form-label">Status: <span style="color:red">*</span></label><br />

            @foreach($status as $aStatus)
            <div class="form-check form-check-inline">
              <label class="form-check-label">
                <input type="radio" class="form-check-input" name="status" id="optionsRadios6" value="{{$aStatus}}" required>
                {{$aStatus}}
              </label>
            </div>
            @endforeach

          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Add Role</button>
        </div>
      </form>
    </div>
  </div>
</div>

@foreach ($roles as $role)
<!-- Role Delete Modal -->
<div class="modal fade" id="deleteRoleModal{{$role->id}}" tabindex="-1" role="dialog" aria-labelledby="deleteRoleModal{{$role->id}}Label" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="deleteRoleModal{{$role->id}}Label">Are you sure you want to delete {{$role->name}}?</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-footer">
        <a type="button" class="btn btn-secondary" data-dismiss="modal">Close</a>
        <a href="{{route('roles.destroy',$role->id)}}" class="btn btn-primary">Delete</a>
      </div>
    </div>
  </div>
</div>

@endforeach


@endsection