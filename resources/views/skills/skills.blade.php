@extends('layout.master')

@section('content')

<div class="d-flex justify-content-between align-items-center flex-wrap grid-margin">
    <div>
        <h4 class="mb-3 mb-md-0">Skills</h4>
    </div>
    <div class="d-flex align-items-center flex-wrap text-nowrap">
    <button type="button" class="btn btn-primary btn-icon-text mb-2 mb-md-0" data-toggle="modal" data-target="#addSkillModal">
      <i class="btn-icon-prepend" data-feather="plus"></i>
      Add Skill
    </button>
  </div>
</div>


<div class="row">
    <div class="col-md-6 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
                <h6 class="card-title">Published Skills</h6>
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            <div style="display:none;">{{$i=1}}</div>
                            @foreach ($published_skills as $pub)
                            <tr>
                                <td>{{$i++}}</td>
                                <td>{{$pub->skillName}}</td>
                                <td>
                                    @can('update',$pub)
                                    <a href="{{route('skills.publish',$pub->skillName)}}" class="btn btn-primary">UnPublish</a>
                                    @endcan
                                </td>
                                <td>
                                <div class="dropdown mb-2">
                                     <button class="btn p-0" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                       <i class="icon-lg text-muted pb-3px" data-feather="more-horizontal"></i>
                                    </button>
                                      <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                        <a class="dropdown-item d-flex align-items-center" href="{{route('skills.edit',$pub->skillName)}}"><i data-feather="edit-2" class="icon-sm mr-2"></i> <span class="">Edit</span></a>
                                         <a class="dropdown-item d-flex align-items-center" href="#" data-toggle="modal" data-target="#deleteSkillModal{{$pub->skillName}}"><i data-feather="trash" class="icon-sm mr-2"></i> <span class="">Delete</span></a>
                                     </div>
                                    </div>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-6 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
                <h6 class="card-title">UnPublished Skills</h6>
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            <div style="display:none;">{{$i=1}}</div>
                            @foreach ($unpublished_skills as $unpub)
                            <tr>
                                <td>{{$i++}}</td>
                                <td>{{$unpub->name}}</td>
                                <td>
                                    @can('update',$unpub)
                                    <a href="{{route('skills.publish',$unpub->name)}}" class="btn btn-primary">Publish</a>
                                    @endcan
                                </td>
                                <td>
                                <div class="dropdown mb-2">
                                     <button class="btn p-0" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                       <i class="icon-lg text-muted pb-3px" data-feather="more-horizontal"></i>
                                    </button>
                                      <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                         <a class="dropdown-item d-flex align-items-center" href="#" data-toggle="modal" data-target="#deleteSkillModal{{$unpub->name}}"><i data-feather="trash" class="icon-sm mr-2"></i> <span class="">Delete</span></a>
                                     </div>
                                    </div>
  
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

</div>




<!-- Add Skill Modal -->
<div class="modal fade" id="addSkillModal" tabindex="-1" role="dialog" aria-labelledby="addSkillModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="addSkillModalLabel">Add Skill</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="{{route('skills.store')}}" method="post">
        @csrf
        <div class="modal-body">
          <div class="form-group">
            <label for="skill-name" class="col-form-label">Name: <span style="color:red">*</span></label>
            <input type="text" name="name" class="form-control" id="skill-name" required>
          </div>
          <div class="form-group">
            <label for="skill-level" class="col-form-label">Level: <span style="color:red">*</span></label>
            <input type="number" name="level" class="form-control" id="skill-level" required>
          </div>
          <div class="form-group">
            <label for="skill-description" class="col-form-label">Description: <span style="color:red">*</span></label>
            <input type="text" name="description" class="form-control" id="skill-description" required>
          </div>
          <div class="form-group">
            <label for="skill-last_use" class="col-form-label">last_use: <span style="color:red">*</span></label>
            <input type="date" name="last_use" class="form-control" id="skill-last_use" required>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Add Skill</button>
        </div>
      </form>
    </div>
  </div>
</div>



<!-- Skill Delete Modal -->

@foreach ($published_skills as $pub)
<div class="modal fade" id="deleteSkillModal{{$pub->skillName}}" tabindex="-1" role="dialog" aria-labelledby="deleteSkillModal{{$pub->skillName}}Label" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="deleteSkillModal{{$pub->skillName}}Label">Are you sure you want to delete {{$pub->skillName}}?</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-footer">
        <a type="button" class="btn btn-secondary" data-dismiss="modal">Close</a>
        <a href="{{route('skills.destroy',$pub->skillName)}}" class="btn btn-primary">Delete</a>
      </div>
    </div>
  </div>
</div>

@endforeach


@foreach ($unpublished_skills as $unpub)
<div class="modal fade" id="deleteSkillModal{{$unpub->name}}" tabindex="-1" role="dialog" aria-labelledby="deleteSkillModal{{$unpub->name}}Label" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="deleteSkillModal{{$unpub->name}}Label">Are you sure you want to delete {{$unpub->name}}?</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-footer">
        <a type="button" class="btn btn-secondary" data-dismiss="modal">Close</a>
        <a href="{{route('skills.destroy',$unpub->name)}}" class="btn btn-primary">Delete</a>
      </div>
    </div>
  </div>
</div>

@endforeach

@endsection
