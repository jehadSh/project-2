@extends('layout.master')

@section('content')
@foreach ($skills as $skill)

<nav class="page-breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{route('skills')}}">skills</a></li>
        <li class="breadcrumb-item"><a href="#">{{$skill->name}}</a></li>
    </ol>
</nav>
<div class="row">
    <div class="col-lg-12 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">{{$skill->name}}</h4>
                <form action="{{route('skills.store')}}" method="post">
                    @csrf
                    <input type="hidden" name="id" value="{{$skill->id}}">
                    <div class="form-group">
                        <label for="skill-name" class="col-form-label">Name:</label>
                        <input type="text" name="name" class="form-control" id="skill-name" value="{{$skill->name}}" required>
                    </div>

                    <div class="form-group">
                       <label for="skill-level" class="col-form-label">Level: <span style="color:red">*</span></label>
                       <input type="number" name="level" class="form-control" id="skill-level" value="{{$skill->level}}"required>
                    </div>
                    <div class="form-group">
                         <label for="skill-description" class="col-form-label">Description: <span style="color:red">*</span></label>
                         <input type="text" name="description" class="form-control" id="skill-description" value="{{$skill->description}}"required>
                     </div>
                     <div class="form-group">
                        <label for="skill-last_use" class="col-form-label">last_use: <span style="color:red">*</span></label>
                        <input type="date" name="last_use" class="form-control" id="skill-last_use" value="{{$skill->last_use}}"required>
                     </div>


                    <div class="table-responsive">
                        <table class="table table-striped">
                            <tbody>
                                <tr>
                                    <td>
                                        <button type="submit" class="btn btn-primary">Submit</button>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>


@endforeach
@endsection