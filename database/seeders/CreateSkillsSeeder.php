<?php

namespace Database\Seeders;


use App\Models\Skill;
use Illuminate\Database\Seeder;

class CreateSkillsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $skill = [
            [
                'profile_id' => '1',
                'name' => 'Laravel',
                'description' => 'beginner',
                'last_use' => '2021-10-12',
                'level' => '3',
                'evaluation' => '3',
                'evaluation_date' => '2021-10-12',
                'isAccepted' => '1',
                'publish_name' => '1',
                'created_at'=>'2021-10-12',
                'updated_at'=>'2021-10-12'
            ],
            [
                'profile_id' => '1',
                'name' => 'Angular',
                'description' => 'beginner',
                'last_use' => '2021-10-12',
                'level' => '1',
                'evaluation' => '3',
                'evaluation_date' => '2021-10-12',
                'isAccepted' => '1',
                'publish_name' => '1',
                'created_at'=>'2021-10-12',
                'updated_at'=>'2021-10-12'
            ],
            [
                'profile_id' => '2',
                'name' => 'flutter',
                'description' => 'beginner',
                'last_use' => '2021-10-12',
                'level' => '2',
                'evaluation' => '3',
                'evaluation_date' => '2021-10-12',
                'publish_name' => '1',
                'isAccepted' => '1',
                'created_at'=>'2021-10-12',
                'updated_at'=>'2021-10-12'
            ],
            [
                'profile_id' => '3',
                'name' => 'flutter',
                'description' => 'beginner',
                'last_use' => '2021-10-12',
                'level' => '5',
                'evaluation' => '3',
                'evaluation_date' => '2021-10-12',
                'publish_name' => '1',
                'isAccepted' => '1',
                'created_at'=>'2021-10-12',
                'updated_at'=>'2021-10-12'
            ],
        ];
        Skill::insert($skill);
    }
}

