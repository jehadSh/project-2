<?php

namespace Database\Seeders;

use App\Models\Task;
use App\Models\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Database\Seeder;

class CreateTasksSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = [
            [
                'profile_id' => null,
                'created_by' => 1,
                //'promoted_by' => null,
                'project_id' => 1,
                'title' => 'one',
                'description' => 'simple',
                'skills' => 'laravel',
                'start_at' => '2021-10-12',
                'actual_start' => '2021-10-12',
                'end_at' => '2021-10-12',
                'actual_end' => '2021-10-12',
               //'deadline' => '2021-10-12',
                'hours' => 2,
              //  'actual_hours' => 2,
                'min_grade' => 2,
             //   'per-requirement' => 'local',
                'evaluation' => 2,
                'evaluation_date' => '2021-10-12',
                'evaluated_by' => 1,
                'created_at'=>'2021-10-12',
                'updated_at'=>'2021-10-12'
            ],
            [
                'profile_id' => null,
                'created_by' => 1,
             //   'promoted_by' => null,
                'project_id' => 1,
                'title' => 'second',
                'description' => 'simple',
                'skills' => 'Angular',
                'start_at' => '2021-10-12',
                'actual_start' => '2021-10-12',
                'end_at' => '2021-10-12',
                'actual_end' => '2021-10-12',
           //     'deadline' => '2021-10-12',
                'hours' => 2,
            //    'actual_hours' => 2,
                'min_grade' => 2,
             //   'per-requirement' => 'local',
                'evaluation' => 2,
                'evaluation_date' => '2021-10-12',
                'evaluated_by' => 1,
                'created_at'=>'2021-10-12',
                'updated_at'=>'2021-10-12'
            ],
            [
                'profile_id' => null,
                'created_by' => 1,
            //   'promoted_by' => 1,
                'project_id' => 2,
                'title' => 'third',
                'description' => 'simple',
                'skills' => 'flutter',
                'start_at' => '2021-10-12',
                'actual_start' => '2021-10-12',
                'end_at' => '2021-10-12',
                'actual_end' => '2021-10-12',
            //    'deadline' => '2021-10-12',
                'hours' => 2,
            //    'actual_hours' => 2,
                'min_grade' => 2,
            //    'per-requirement' => 'local',
                'evaluation' => 2,
                'evaluation_date' => '2021-10-12',
                'evaluated_by' => 1,
                'created_at'=>'2021-10-12',
                'updated_at'=>'2021-10-12'
            ]
        ];


            Task::insert($user);

    }
}
