<?php

namespace Database\Seeders;

use App\Models\Project;
use Illuminate\Database\Seeder;

class CreateProjectsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $projects = [
            [
                'manager_id' => 1,
                'previous_manager_id' => 1,
                'created_by' => 1,
                'color' => 'red',
                'title' => 'Robin',
                'description' => 'It is an projects to Reservation for the shipment of goods and the organization of financial operations',
                'start_date' => '2021-10-12',
                'estimated_date' => '2022-10-12',
                'actual_date' => '2022-11-12',
                'skills' => 'laravel',
                'datafile_name' => 'local',
                'valid_to' => '2021-10-12',
                'isPublic' => '0',
                'created_at' => '2021-10-12',
                'updated_at' => '2021-10-12'

            ],
            [
                'manager_id' => 2,
                'previous_manager_id' => 1,
                'created_by' => 1,
                'color' => 'blue',
                'title' => 'Work Space',
                'description' => 'Program projects management system, it is Manage tasks, members, and groups to save and use resources correctly and project workflow monitoring. Developed By Laravel with Angular & Flutter and mySql for database.',
                'start_date' => '2021-6-1',
                'estimated_date' => '2022-5-1',
                'actual_date' => '2022-6-1',
                'skills' => 'flutter',
                'datafile_name' => 'local',
                'valid_to' => '2021-10-12',
                'isPublic' => '0',
                'created_at' => '2021-10-12',
                'updated_at' => '2021-10-12'
            ],
            [
                'manager_id' => 1,
                'previous_manager_id' => 1,
                'created_by' => 1,
                'color' => 'black',
                'title' => 'alhifaz',
                'description' => 'It is an projects to organize the process of recitation and testing (the Holy Quran) between students and teachers.',
                'start_date' => '2021-8-18',
                'estimated_date' => '2022-8-17',
                'actual_date' => '2022-9-12',
                'skills' => 'Angular',
                'datafile_name' => 'local',
                'valid_to' => '2021-10-12',
                'isPublic' => '1',
                'created_at' => '2021-10-12',
                'updated_at' => '2021-10-12'
            ]
        ];


        Project::insert($projects);


    }
}
