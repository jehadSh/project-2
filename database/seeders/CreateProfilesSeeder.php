<?php

namespace Database\Seeders;

use App\Models\Profile;
use Illuminate\Database\Seeder;

class CreateProfilesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $profile = [
            [
                'user_id' => '1',
                'grade'=>'7',
                'gender'=>'male',
                'birthday'=>'2021-10-12',
                'city'=>'Damascus',
                'weekly_hours'=>'24',
                'website'=>'https://www.website.com/',
                'github'=>'https://www.github.com/',
                'gitlab'=>'https://www.gitlab.com/',
                'social_media'=>'https://www.social_media.com/',
                'picture'=>'https://www.clipartmax.com/png/middle/211-2111181_businessman-profession-cartoon-businessman-cartoon-png.png',
                'cover'=>'https://upload.wikimedia.org/wikipedia/commons/thumb/3/35/Location_dot_blue.svg/1200px-Location_dot_blue.svg.png',
                'status'=>'active',
                'created_at'=>'2021-10-12',
                'updated_at'=>'2021-10-12'
            ],
            [
                'user_id' => '2',
                'grade'=>'5',
                'gender'=>'male',
                'birthday'=>'2021-10-20',
                'city'=>'Homes',
                'weekly_hours'=>'30',
                'website'=>'https://www.website.com/',
                'github'=>'https://www.github.com/',
                'gitlab'=>'https://www.gitlab.com/',
                'social_media'=>'https://www.social_media.com/',
                'picture'=>'https://www.clipartmax.com/png/middle/211-2111181_businessman-profession-cartoon-businessman-cartoon-png.png',
                'cover'=>'https://upload.wikimedia.org/wikipedia/commons/thumb/3/35/Location_dot_blue.svg/1200px-Location_dot_blue.svg.png',
                'status'=>'active',
                'created_at'=>'2021-10-12',
                'updated_at'=>'2021-10-12'
            ],
            [
                'user_id' => '3',
                'grade'=>'3',
                'gender'=>'male',
                'birthday'=>'2021-10-20',
                'city'=>'Hma',
                'weekly_hours'=>'30',
                'website'=>'https://www.website.com/',
                'github'=>'https://www.github.com/',
                'gitlab'=>'https://www.gitlab.com/',
                'social_media'=>'https://www.social_media.com/',
                'picture'=>'https://www.clipartmax.com/png/middle/211-2111181_businessman-profession-cartoon-businessman-cartoon-png.png',
                'cover'=>'https://upload.wikimedia.org/wikipedia/commons/thumb/3/35/Location_dot_blue.svg/1200px-Location_dot_blue.svg.png',
                'status'=>'active',
                'created_at'=>'2021-10-12',
                'updated_at'=>'2021-10-12'
            ],
            [
                'user_id' => '4',
                'grade'=>'7',
                'gender'=>'female',
                'birthday'=>'2000-10-20',
                'city'=>'Sweida',
                'weekly_hours'=>'25',
                'website'=>'https://www.website.com/',
                'github'=>'https://www.github.com/',
                'gitlab'=>'https://www.gitlab.com/',
                'social_media'=>'https://www.social_media.com/',
                'picture'=>'https://www.clipartmax.com/png/middle/211-2111181_businessman-profession-cartoon-businessman-cartoon-png.png',
                'cover'=>'https://upload.wikimedia.org/wikipedia/commons/thumb/3/35/Location_dot_blue.svg/1200px-Location_dot_blue.svg.png',
                'status'=>'active',
                'created_at'=>'2021-10-12',
                'updated_at'=>'2021-10-12'
            ],
        ];

            Profile::insert($profile);

    }
}
