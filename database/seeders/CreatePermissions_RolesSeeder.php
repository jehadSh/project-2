<?php

namespace Database\Seeders;

use App\Models\Permission_Role;
use Illuminate\Database\Seeder;

class CreatePermissions_RolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permission = [
            //////////////////////////////////////////////
            //Super Admin
            //////////////////////////////////////////////

            //permission Profile
            [
                'role_id'=>'1',
                'permission_id'=>'1'
            ],
            [
                'role_id'=>'1',
                'permission_id'=>'2'
            ],
            [
                'role_id'=>'1',
                'permission_id'=>'3'
            ],
            [
                'role_id'=>'1',
                'permission_id'=>'4'
            ],
            [
                'role_id'=>'1',
                'permission_id'=>'5'
            ],
            //permission Project
            [
                'role_id'=>'1',
                'permission_id'=>'8'
            ],
            [
                'role_id'=>'1',
                'permission_id'=>'9'
            ],
            [
                'role_id'=>'1',
                'permission_id'=>'10'
            ],
            [
                'role_id'=>'1',
                'permission_id'=>'11'
            ],
            [
                'role_id'=>'1',
                'permission_id'=>'12'
            ],

            //permission Education
            [
                'role_id'=>'1',
                'permission_id'=>'15'
            ],
            [
                'role_id'=>'1',
                'permission_id'=>'16'
            ],
            [
                'role_id'=>'1',
                'permission_id'=>'17'
            ],
            [
                'role_id'=>'1',
                'permission_id'=>'18'
            ],
            [
                'role_id'=>'1',
                'permission_id'=>'19'
            ],

            //permission Skill
            [
                'role_id'=>'1',
                'permission_id'=>'22'
            ],
            [
                'role_id'=>'1',
                'permission_id'=>'23'
            ],
            [
                'role_id'=>'1',
                'permission_id'=>'24'
            ],
            [
                'role_id'=>'1',
                'permission_id'=>'25'
            ],
            [
                'role_id'=>'1',
                'permission_id'=>'26'
            ],


            //permission Task
            [
                'role_id'=>'1',
                'permission_id'=>'29'
            ],
            [
                'role_id'=>'1',
                'permission_id'=>'30'
            ],
            [
                'role_id'=>'1',
                'permission_id'=>'31'
            ],
            [
                'role_id'=>'1',
                'permission_id'=>'32'
            ],
            [
                'role_id'=>'1',
                'permission_id'=>'33'
            ],

        ];

        Permission_Role::insert($permission);

    }
}
