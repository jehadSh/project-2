<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Database\Seeder;

class CreateUsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = [
            [
                'name' => 'Jehad',
                'email' => 'jehad.shekhzain@gmail.com',
                'password' => Hash::make('12345678'),
                'phone' => ('0934151507'),
                'status' => 'active',
                'created_at'=>'2021-10-12',
                'updated_at'=>'2021-10-12'
            ],
            [
                'name' => 'mohamed',
                'email' => 'mohamed@gmail.com',
                'password' => Hash::make('12345678'),
                'phone' => ('0949224913'),
                'status' => 'active',
                'created_at'=>'2021-10-12',
                'updated_at'=>'2021-10-12'
            ],
            [
                'name' => 'osama',
                'email' => 'osama@gmail.com',
                'password' => Hash::make('12345678'),
                'phone' => ('09492249135'),
                'status' => 'inactive',
                'created_at'=>'2021-10-12',
                'updated_at'=>'2021-10-12'
            ],
            [
                'name' => 'gahdeer',
                'email' => 'gahdeer@gmail.com',
                'password' => Hash::make('12345678'),
                'phone' => ('0949224913599'),
                'status' => 'inactive',
                'created_at'=>'2021-10-12',
                'updated_at'=>'2021-10-12'
            ]
        ];

            User::insert($user);

    }
}
