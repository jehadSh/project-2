<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
        $this->call(CreateUsersSeeder::class);
        $this->call(CreateRolesSeeder::class);
        $this->call(CreatePermissionsSeeder::class);
        $this->call(CreateProfilesSeeder::class);
        $this->call(CreateUser_RolesSeeder::class);
        $this->call(CreatePermissions_RolesSeeder::class);
        $this->call(CreateProjectsSeeder::class);
        $this->call(CreateTasksSeeder::class);
        $this->call(CreateGroupsSeeder::class);
        $this->call(CreateSkillsSeeder::class);
        $this->call(CreateSkillNameSeeder::class);


    }
}
