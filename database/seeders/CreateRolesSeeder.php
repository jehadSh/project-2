<?php

namespace Database\Seeders;

use App\Models\Role;
use Illuminate\Database\Seeder;

class CreateRolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role = [
            [
                'name'=>'Super Admin',
                'alias'=>'superAdmin',
                'status' => 'publish',
                'created_at'=>'2021-10-12',
                'updated_at'=>'2021-10-12'
            ],
            [
                'name'=>'Project Maneger',
                'alias'=>'projectManager',
                'status' => 'publish',
                'created_at'=>'2021-10-12',
                'updated_at'=>'2021-10-12'
            ],
            [
                'name'=>'Developer',
                'alias'=>'developer',
                'status' => 'publish',
                'created_at'=>'2021-10-12',
                'updated_at'=>'2021-10-12'
            ],
        ];


            Role::insert($role);

    }
}
