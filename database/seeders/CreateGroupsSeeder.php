<?php

namespace Database\Seeders;

use App\Models\Country;
use App\Models\Group;
use Illuminate\Support\Facades\Hash;
use Illuminate\Database\Seeder;

class CreateGroupsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $group = [
            [
                'project_id' => '1',
                'profile_id' => '1',
                'assigned_by' => '1',
                'role' => 'team player',
                'created_at'=>'2021-10-12',
                'updated_at'=>'2021-10-12'
            ],
            [
                'project_id' => '2',
                'profile_id' => '1',
                'assigned_by' => '1',
                'role' => 'team player',
                'created_at'=>'2021-10-12',
                'updated_at'=>'2021-10-12'
            ],
            [
                'project_id' => '1',
                'profile_id' => '2',
                'assigned_by' => '1',
                'role' => 'team player',
                'created_at'=>'2021-10-12',
                'updated_at'=>'2021-10-12'
            ],
            [
                'project_id' => '1',
                'profile_id' => '3',
                'assigned_by' => '1',
                'role' => 'team player',
                'created_at'=>'2021-10-12',
                'updated_at'=>'2021-10-12'
            ],
            [
                'project_id' => '2',
                'profile_id' => '2',
                'assigned_by' => '1',
                'role' => 'team player',
                'created_at'=>'2021-10-12',
                'updated_at'=>'2021-10-12'
            ],
        ];
        Group::insert($group);
    }
}

