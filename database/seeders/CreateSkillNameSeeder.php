<?php

namespace Database\Seeders;

use App\Models\CommonSkill;
use Illuminate\Support\Facades\Hash;
use Illuminate\Database\Seeder;

class CreateSkillNameSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $commonSkill = [
            [
                'skillName' => 'flutter',
            ],
            [
                'skillName' => 'Laravel',
            ],
            [
                'skillName' => 'Angular',
            ],
        ];

        CommonSkill::insert($commonSkill);

    }
}
