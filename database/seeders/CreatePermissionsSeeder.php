<?php

namespace Database\Seeders;

use App\Models\Permission;
use Illuminate\Database\Seeder;

class CreatePermissionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        /*
            profile 1->7

            project 8->14

            Education 15->21

            skill 22->28

            task  29->35
        */
        $permission = [
            // Start Profile //
            [
                'name' => 'viewAny',
                'alias' => 'viewAnyProfile',
                'model' => 'Profile',
                'status' => 'publish',
            ],
            [
                'name' => 'view',
                'alias' => 'viewProfile',
                'model' => 'Profile',
                'status' => 'publish',
            ],
            [
                'name' => 'create',
                'alias' => 'createProfile',
                'model' => 'Profile',
                'status' => 'publish',
            ],
            [
                'name' => 'update',
                'alias' => 'updateProfile',
                'model' => 'Profile',
                'status' => 'publish',
            ],
            [
                'name' => 'delete',
                'alias' => 'deleteProfile',
                'model' => 'Profile',
                'status' => 'publish',
            ],
            [
                'name' => 'restore',
                'alias' => 'restoreProfile',
                'model' => 'Profile',
                'status' => 'publish',
            ],
            [
                'name' => 'forceDelete',
                'alias' => 'forceDeleteProfile',
                'model' => 'Profile',
                'status' => 'publish',
            ],
            // End Profile //

            //Start Project //
            [
                'name' => 'viewAny',
                'alias' => 'viewAnyProject',
                'model' => 'Project',
                'status' => 'publish',
            ],
            [
                'name' => 'view',
                'alias' => 'viewProject',
                'model' => 'Project',
                'status' => 'publish',
            ],
            [
                'name' => 'create',
                'alias' => 'createProject',
                'model' => 'Project',
                'status' => 'publish',
            ],
            [
                'name' => 'update',
                'alias' => 'updateProject',
                'model' => 'Project',
                'status' => 'publish',
            ],
            [
                'name' => 'delete',
                'alias' => 'deleteProject',
                'model' => 'Project',
                'status' => 'publish',
            ],
            [
                'name' => 'restore',
                'alias' => 'restoreProject',
                'model' => 'Project',
                'status' => 'publish',
            ],
            [
                'name' => 'forceDelete',
                'alias' => 'forceDeleteProject',
                'model' => 'Project',
                'status' => 'publish',
            ],
            //End Project //

            //Start Education //
            [
                'name' => 'viewAny',
                'alias' => 'viewAnyEducation',
                'model' => 'Education',
                'status' => 'publish',
            ],
            [
                'name' => 'view',
                'alias' => 'viewEducation',
                'model' => 'Education',
                'status' => 'publish',
            ],
            [
                'name' => 'create',
                'alias' => 'createEducation',
                'model' => 'Education',
                'status' => 'publish',
            ],
            [
                'name' => 'update',
                'alias' => 'updateEducation',
                'model' => 'Education',
                'status' => 'publish',
            ],
            [
                'name' => 'delete',
                'alias' => 'deleteEducation',
                'model' => 'Education',
                'status' => 'publish',
            ],
            [
                'name' => 'restore',
                'alias' => 'restoreEducation',
                'model' => 'Education',
                'status' => 'publish',
            ],
            [
                'name' => 'forceDelete',
                'alias' => 'forceDeleteEducation',
                'model' => 'Education',
                'status' => 'publish',
            ],
            //End Education //

            //Start Skill //
            [
                'name' => 'viewAny',
                'alias' => 'viewAnySkill',
                'model' => 'Skill',
                'status' => 'publish',
            ],
            [
                'name' => 'view',
                'alias' => 'viewSkill',
                'model' => 'Skill',
                'status' => 'publish',
            ],
            [
                'name' => 'create',
                'alias' => 'createSkill',
                'model' => 'Skill',
                'status' => 'publish',
            ],
            [
                'name' => 'update',
                'alias' => 'updateSkill',
                'model' => 'Skill',
                'status' => 'publish',
            ],
            [
                'name' => 'delete',
                'alias' => 'deleteSkill',
                'model' => 'Skill',
                'status' => 'publish',
            ],
            [
                'name' => 'restore',
                'alias' => 'restoreSkill',
                'model' => 'Skill',
                'status' => 'publish',
            ],
            [
                'name' => 'forceDelete',
                'alias' => 'forceDeleteSkill',
                'model' => 'Skill',
                'status' => 'publish',
            ],
            //End Skill //

            //Start Task //
            [
                'name' => 'viewAny',
                'alias' => 'viewAnyTask',
                'model' => 'Task',
                'status' => 'publish',
            ],
            [
                'name' => 'view',
                'alias' => 'viewTask',
                'model' => 'Task',
                'status' => 'publish',
            ],
            [
                'name' => 'create',
                'alias' => 'createTask',
                'model' => 'Task',
                'status' => 'publish',
            ],
            [
                'name' => 'update',
                'alias' => 'updateTask',
                'model' => 'Task',
                'status' => 'publish',
            ],
            [
                'name' => 'delete',
                'alias' => 'deleteTask',
                'model' => 'Task',
                'status' => 'publish',
            ],
            [
                'name' => 'restore',
                'alias' => 'restoreTask',
                'model' => 'Task',
                'status' => 'publish',
            ],
            [
                'name' => 'forceDelete',
                'alias' => 'forceDeleteTask',
                'model' => 'Task',
                'status' => 'publish',
            ],
            //End Task //


            //Start Role //
            [
                'name' => 'viewAny',
                'alias' => 'viewAnyRole',
                'model' => 'Role',
                'status' => 'publish',
            ],
            [
                'name' => 'view',
                'alias' => 'viewRole',
                'model' => 'Role',
                'status' => 'publish',
            ],
            [
                'name' => 'create',
                'alias' => 'createRole',
                'model' => 'Role',
                'status' => 'publish',
            ],
            [
                'name' => 'update',
                'alias' => 'updateRole',
                'model' => 'Role',
                'status' => 'publish',
            ],
            [
                'name' => 'delete',
                'alias' => 'deleteRole',
                'model' => 'Role',
                'status' => 'publish',
            ],
            [
                'name' => 'restore',
                'alias' => 'restoreRole',
                'model' => 'Role',
                'status' => 'publish',
            ],
            [
                'name' => 'forceDelete',
                'alias' => 'forceDeleteRole',
                'model' => 'Role',
                'status' => 'publish',
            ],
            // End Role //

            // Start User//

            [
                'name' => 'viewAny',
                'alias' => 'viewAnyUser',
                'model' => 'User',
                'status' => 'publish',
            ],
            [
                'name' => 'view',
                'alias' => 'viewUser',
                'model' => 'User',
                'status' => 'publish',
            ],
            [
                'name' => 'create',
                'alias' => 'createUser',
                'model' => 'User',
                'status' => 'publish',
            ],
            [
                'name' => 'update',
                'alias' => 'updateUser',
                'model' => 'User',
                'status' => 'publish',
            ],
            [
                'name' => 'delete',
                'alias' => 'deleteUser',
                'model' => 'User',
                'status' => 'publish',
            ],
            [
                'name' => 'restore',
                'alias' => 'restoreUser',
                'model' => 'User',
                'status' => 'publish',
            ],
            [
                'name' => 'forceDelete',
                'alias' => 'forceDeleteUser',
                'model' => 'User',
                'status' => 'publish',
            ],
            //End User //

            //Start Group //
            [
                'name' => 'viewAny',
                'alias' => 'viewAnyGroup',
                'model' => 'Group',
                'status' => 'publish',
            ],
            [
                'name' => 'view',
                'alias' => 'viewGroup',
                'model' => 'Group',
                'status' => 'publish',
            ],
            [
                'name' => 'create',
                'alias' => 'createGroup',
                'model' => 'Group',
                'status' => 'publish',
            ],
            [
                'name' => 'update',
                'alias' => 'updateGroup',
                'model' => 'Group',
                'status' => 'publish',
            ],
            [
                'name' => 'delete',
                'alias' => 'deleteGroup',
                'model' => 'Group',
                'status' => 'publish',
            ],
            [
                'name' => 'restore',
                'alias' => 'restoreGroup',
                'model' => 'Group',
                'status' => 'publish',
            ],
            [
                'name' => 'forceDelete',
                'alias' => 'forceDeleteGroup',
                'model' => 'Group',
                'status' => 'publish',
            ],
            // End Group //


            //Start  File//
            [
                'name' => 'viewAny',
                'alias' => 'viewAnyFile',
                'model' => 'File',
                'status' => 'publish',
            ],
            [
                'name' => 'view',
                'alias' => 'viewFile',
                'model' => 'File',
                'status' => 'publish',
            ],
            [
                'name' => 'create',
                'alias' => 'createFile',
                'model' => 'File',
                'status' => 'publish',
            ],
            [
                'name' => 'update',
                'alias' => 'updateFile',
                'model' => 'File',
                'status' => 'publish',
            ],
            [
                'name' => 'delete',
                'alias' => 'deleteFile',
                'model' => 'File',
                'status' => 'publish',
            ],
            [
                'name' => 'restore',
                'alias' => 'restoreFile',
                'model' => 'File',
                'status' => 'publish',
            ],
            [
                'name' => 'forceDelete',
                'alias' => 'forceDeleteFile',
                'model' => 'File',
                'status' => 'publish',
            ],
            // End File //

            //Start  Note//
            [
                'name' => 'viewAny',
                'alias' => 'viewAnyNote',
                'model' => 'Note',
                'status' => 'publish',
            ],
            [
                'name' => 'view',
                'alias' => 'viewNote',
                'model' => 'Note',
                'status' => 'publish',
            ],
            [
                'name' => 'create',
                'alias' => 'createNote',
                'model' => 'Note',
                'status' => 'publish',
            ],
            [
                'name' => 'update',
                'alias' => 'updateNote',
                'model' => 'Note',
                'status' => 'publish',
            ],
            [
                'name' => 'delete',
                'alias' => 'deleteNote',
                'model' => 'Note',
                'status' => 'publish',
            ],
            [
                'name' => 'restore',
                'alias' => 'restoreNote',
                'model' => 'Note',
                'status' => 'publish',
            ],
            [
                'name' => 'forceDelete',
                'alias' => 'forceDeleteNote',
                'model' => 'Note',
                'status' => 'publish',
            ],
            // End Note //
            //Start  Comment//
            [
                'name' => 'viewAny',
                'alias' => 'viewAnyComment',
                'model' => 'Comment',
                'status' => 'publish',
            ],
            [
                'name' => 'view',
                'alias' => 'viewComment',
                'model' => 'Comment',
                'status' => 'publish',
            ],
            [
                'name' => 'create',
                'alias' => 'createComment',
                'model' => 'Comment',
                'status' => 'publish',
            ],
            [
                'name' => 'update',
                'alias' => 'updateComment',
                'model' => 'Comment',
                'status' => 'publish',
            ],
            [
                'name' => 'delete',
                'alias' => 'deleteComment',
                'model' => 'Comment',
                'status' => 'publish',
            ],
            [
                'name' => 'restore',
                'alias' => 'restoreComment',
                'model' => 'Comment',
                'status' => 'publish',
            ],
            [
                'name' => 'forceDelete',
                'alias' => 'forceDeleteComment',
                'model' => 'Comment',
                'status' => 'publish',
            ],
            // End Comment //
            //Start  Notification//
            [
                'name' => 'viewAny',
                'alias' => 'viewAnyNotification',
                'model' => 'Notification',
                'status' => 'publish',
            ],
            [
                'name' => 'view',
                'alias' => 'viewNotification',
                'model' => 'Notification',
                'status' => 'publish',
            ],
            [
                'name' => 'create',
                'alias' => 'createNotification',
                'model' => 'Notification',
                'status' => 'publish',
            ],
            [
                'name' => 'update',
                'alias' => 'updateNotification',
                'model' => 'Notification',
                'status' => 'publish',
            ],
            [
                'name' => 'delete',
                'alias' => 'deleteNotification',
                'model' => 'Notification',
                'status' => 'publish',
            ],
            [
                'name' => 'restore',
                'alias' => 'restoreNotification',
                'model' => 'Notification',
                'status' => 'publish',
            ],
            [
                'name' => 'forceDelete',
                'alias' => 'forceDeleteNotification',
                'model' => 'Notification',
                'status' => 'publish',
            ],
            // End Notification //
            //Start  Applicant//
            [
                'name' => 'viewAny',
                'alias' => 'viewAnyApplicant',
                'model' => 'Applicant',
                'status' => 'publish',
            ],
            [
                'name' => 'view',
                'alias' => 'viewApplicant',
                'model' => 'Applicant',
                'status' => 'publish',
            ],
            [
                'name' => 'create',
                'alias' => 'createApplicant',
                'model' => 'Applicant',
                'status' => 'publish',
            ],
            [
                'name' => 'update',
                'alias' => 'updateApplicant',
                'model' => 'Applicant',
                'status' => 'publish',
            ],
            [
                'name' => 'delete',
                'alias' => 'deleteApplicant',
                'model' => 'Applicant',
                'status' => 'publish',
            ],
            [
                'name' => 'restore',
                'alias' => 'restoreApplicant',
                'model' => 'Applicant',
                'status' => 'publish',
            ],
            [
                'name' => 'forceDelete',
                'alias' => 'forceDeleteApplicant',
                'model' => 'Applicant',
                'status' => 'publish',
            ],
            // End Applicant //



        ];

        Permission::insert($permission);
    }
}
