<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('projects', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('manager_id')->unsigned();
            $table->integer('previous_manager_id')->unsigned()->nullable();
            $table->integer('created_by')->unsigned();
            $table->string('color');
            $table->string('title');
            $table->text('description')->nullable();
            $table->longText('skills')->nullable();
            $table->date('start_date')->nullable();
            $table->date('estimated_date')->nullable();
            $table->date('actual_date')->nullable();
            $table->enum('datafile_type',['local','github','gitlab','open end'])->default('local');
            $table->string('datafile_name')->nullable();
            $table->date('valid_to')->nullable();
            $table->boolean('isPublic')->default(0);
            $table->enum('status',['unpublished','none','in progress','done','approved','refused'])->default('none')->nullable();
            $table->softDeletes();
            $table->timestamps();
            $table->foreign('manager_id')->references('id')->on('profiles');
            $table->foreign('previous_manager_id')->references('id')->on('profiles');
            $table->foreign('created_by')->references('id')->on('profiles');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('projects');
    }
}
