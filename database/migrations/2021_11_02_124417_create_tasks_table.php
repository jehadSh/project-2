<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTasksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tasks', function (Blueprint $table) {
            $table->id();
            $table->integer('profile_id')->unsigned()->nullable();
            $table->integer('created_by')->unsigned();
           // $table->integer('promoted_by')->unsigned()->nullable();حذف
            $table->integer('project_id')->unsigned();
            $table->string('title');
            $table->longText('description')->nullable();
            $table->string('skills')->nullable();
            $table->date('start_at')->nullable();
            $table->date('actual_start')->nullable();
            $table->date('end_at')->nullable();
            $table->date('actual_end')->nullable();
            //$table->date('deadline')->nullable();
            $table->double('hours')->nullable();
            //$table->double('actual_hours')->nullable();
            $table->integer('min_grade')->default(1);
            //->string('per-requirement');
            $table->integer('evaluation')->nullable();
            $table->dateTime('evaluation_date')->nullable();
            $table->integer('evaluated_by')->unsigned()->default(1);
            $table->enum('status',['unpublished','none','todo','in progress','completed','failed','cancel'])->default('none');
            $table->softDeletes();
            $table->timestamps();

            $table->foreign('profile_id')->references('id')
                ->on('profiles');

            $table->foreign('created_by')->references('id')
                ->on('profiles');

            /*$table->foreign('promoted_by')->references('id')
                ->on('profiles');*/

            $table->foreign('evaluated_by')->references('id')
                ->on('profiles');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tasks');
    }
}
