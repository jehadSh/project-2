<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSkillsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('skills', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('profile_id')->unsigned()->default(1);
            $table->string('name');
            $table->text('description')->nullable();
            $table->date('last_use');
            $table->integer('level')->nullable();
            $table->integer('evaluation')->nullable();
            $table->dateTime('evaluation_date')->nullable();
            $table->boolean('publish_name')->default(false);
            $table->boolean('isAccepted')->default(false);
            $table->softDeletes();
            $table->timestamps();

            $table->foreign('profile_id')->references('id')->on('profiles');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('skills');
    }
}
