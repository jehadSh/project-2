<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profiles', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->integer('grade')->nullable();
            $table->enum('gender',['female','male'])->nullable();
            $table->date('birthday')->nullable();
            $table->string('city')->nullable();
            $table->integer('weekly_hours')->default(0);
            $table->string('website')->nullable();
            $table->string('github')->nullable();
            $table->string('gitlab')->nullable();
            $table->longText('social_media')->nullable();
            $table->longText('contacts')->nullable();
            $table->string('picture')->nullable();
            $table->string('cover')->nullable();
            $table->enum('status',['active','inactive'])->default('inactive');
            $table->softDeletes();
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('profiles');
    }
}
