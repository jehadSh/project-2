<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateApplicantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('applicants', function (Blueprint $table) {
            $table->bigInteger('task_id')->unsigned();
            $table->integer('profile_id')->unsigned()->nullable();
            $table->primary(['profile_id','task_id']);
            $table->integer('assigned_by')->unsigned()->nullable();
            $table->boolean('approved')->nullable()->default(false);
            $table->timestamps();

             $table->foreign('assigned_by')->references('id')
                 ->on('profiles');
            $table->foreign('profile_id')->references('id')
                ->on('profiles');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('aoolicants');
    }
}
