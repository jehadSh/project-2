<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGroupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('groups', function (Blueprint $table) {
            $table->bigInteger('project_id')->unsigned();
            $table->bigInteger('profile_id')->unsigned();
            $table->unique(['profile_id','project_id']);
            $table->integer('assigned_by')->unsigned();
            $table->enum('role', ['team player', 'organizer', 'observer', 'guest', 'contributor', 'tester'])
                ->default('team player');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('groups');
    }
}
