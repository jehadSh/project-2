<?php

function getModels(){
    return ['Role','User','Profile','Education','Skill','Project','Task','Group','Applicant','File','Comment','Note','Notification'];
}

function getPermissions(){
    return ['viewAny','view','create','update','delete','restore','forceDelete'];
}

function getMainRoles(){
    return ['superAdmin','projectManager','developer'];
}

function getRolesStatus(){
    return ['publish','unpublished'];
}

function getUserStatus(){ 
    return ['active','inactive','band','blocked'];
}

function getProfileStatus(){ 
    return ['active','inactive'];
}

function getProjectStatus(){ 
    return ['unpublished','none','in progress','done','approved','refused'];
}
