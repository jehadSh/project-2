<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\File;
use Illuminate\Http\Request;
use function PHPUnit\Framework\returnArgument;

class FileController extends Controller
{

    public function index()
    {
        //  $this->authorize('viewAny', File::class);
        $files = File::all();
        return response()->json($files, 200);
    }

    public function create()
    {
    }

    public function store(Request $request)
    {
        //  $this->authorize('create', Comment::class);
        $file = new File([
            'fileable_id' => $request->fileable_id,
            'fileable_type' =>$request->fileable_type,
            'profile_id' => $request->user()->profile->id,
            'type' => $request->type,
            'url' => $request->url,
            'name' => $request->name
        ]);
        $file->save();
        return response()->json($file, 201);
    }

    public function show($id)
    {
        $file = File::where('id',$id)->get();
        return response()->json($file, 200);
    }

    public function getFilesForUser(Request $request){
        $files = File::where('profile_id',$request->user()->profile->id)->get();
        return response()->json($files, 200);
    }
    public function destroy($id)
    {
        File::where('id', $id)->delete();
        return response()->json('Done Destroy File', 200);
    }


}
