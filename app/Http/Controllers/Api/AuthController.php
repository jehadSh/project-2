<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\AuthRequest;
use App\Models\Profile;
use App\Models\User;
use App\Models\User_Role;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;


class AuthController extends Controller
{
    /**
     * this for show how is asked to get task
     * @param AuthRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function register(AuthRequest $request)
    {
        $user = User::create($request->all());
        $user->update([
            'password' => bcrypt($request->password),
        ]);
        $token = $user->createToken('my-app-token')->plainTextToken;
        $response = ['user' => $user, 'token' => $token];
        return response()->json($response, 201);
    }

    /**
     * this for show how is asked to get task
     * @param Request $request
     *
     */
    public function login(Request $request)
    {
        if ($request->email == null && $request->phone == null) {
            return response()->json('pleas enter email or phone', 201);
        }
        $fields = $request->validate([
            'email' => 'string',
            'phone' => 'int',
            'password' => 'required|string'
        ]);
        if ($request->email) {
            // Check email
            $user = User::where('email', $fields['email'])->first();
        } else {
            $user = User::where('phone', $fields['phone'])->first();
        }

        // Check password
        if (!$user || !Hash::check($fields['password'], $user->password)) {
            return response([
                'message' => 'Bad creds'
            ], 401);
        }
        $token = $user->createToken('my-app-token')->plainTextToken;

        if ($user->profile) {
            $profile_id = $user->profile->id;
        } else {
            $profile_id = -1;
        }

        $response = [
            'user' => $user,
            'profile_id' => $profile_id,
            'token' => $token
        ];

        return response()->json($response, 201);
    }

    public function logout()
    {
        Auth::guard('api')->user()->tokens()->delete();
        return response()->json('Logged out', 200);
    }

    public function isToken()
    {
        $user = Auth::user();
        if ($user->profile) {
            $profile_id = $user->profile->id;
        }
        else
        {
            $profile_id = -1;
        }
        $token = 'i love jehad';
        $response = [
            'user' => $user,
            'profile_id' => $profile_id,
            'token' => $token
        ];
        if ($user) {
            return response()->json($response, 200);
        } else {
            return response()->json('not login', 404);
        }

    }

}
