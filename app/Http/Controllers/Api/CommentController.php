<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Applicant;
use App\Models\Comment;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use function PHPUnit\Framework\returnArgument;

class CommentController extends Controller
{

    public function index()
    {
        //  $this->authorize('viewAny', Comment::class);
        $comment = Comment::all();
        return response()->json($comment, 200);
    }

    public function create()
    {
    }

    public function store(Request $request)
    {
        //  $this->authorize('create', Comment::class);
        $comment = new Comment([
            'commentable_id' => $request->commentable_id,
            'commentable_type' => $request->commentable_type,
            'profile_id' => $request->user()->profile->id,
            'reply_to' => $request->reply_to,
            'comment' => $request->comment
        ]);
        $comment->save();
        return response()->json($comment, 201);
    }

    public function show($id)
    {
        $comment = Comment::where('id', $id)->get();
        //  $this->authorize('view',$comment);
        return response()->json($comment, 200);
    }

    public function update(Request $request, $id)
    {
        $comment = Comment::where('id', $id)->first()->update($request->all());
        // $this->authorize('update', $comment);
        return response()->json('Done Update Comment', 200);
    }

    public function getCommentsForUser(Request $request)
    {
        $comments = Comment::where('profile_id', $request->user()->profile->id)->get();
        return response()->json($comments, 200);
    }

    public function destroy($id)
    {
        $comment =  Comment::where('id', $id)->delete();
        // $this->authorize('delete', $comment);
        return response()->json('Done Destroy Comment', 200);
    }


}
