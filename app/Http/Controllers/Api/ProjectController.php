<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\ProjectRequest;
use App\Models\Group;
use App\Models\Order;
use App\Models\Profile;
use App\Models\Project;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rules\In;
use PhpParser\Node\Expr\Array_;
use PhpParser\Node\Scalar\String_;
use function React\Promise\Stream\first;
use function RingCentral\Psr7\copy_to_string;

class ProjectController extends Controller
{

    public function index()
    {
        //  $this->authorize('viewAny', Project::class);
        $project = Project::all();
        return response()->json($project, 200);
    }

    public function create()
    {
    }

    public function store(ProjectRequest $request)
    {

        // $this->authorize('create', Project::class);
        //decode the body request
        $body_data = $request->body_data;
        $data = json_decode($body_data, true);
        // add project to table
        $data['manager_id'] = $request->user()->profile->id;
        $data['previous_manager_id'] = $request->user()->profile->id;
        $data['created_by'] = $request->user()->profile->id;
        // return $data->skill_Ids;
        $project = Project::create($data);

        //get all id's and Role's member from request
        $member = collect($data["profile_Ids"]);
        $member = $member->map(function ($member) use ($project, $request) {
            //add project id for every member in this project
            $member['project_id'] = $project->id;
            $member['assigned_by'] = $request->user()->profile->id;
            return $member;
        });
        //insert all member in group table
        Group::insert($member->all());
        return response()->json('Done Store Project', 201);
        /*$project = new Project();
        $project->manager_id = $request->user()->profile->id;
        $project->previous_manager_id = $request->user()->profile->id;
        $project->created_by = $request->user()->profile->id;
        $project->color = $request->color;
        $project->title = $request->title;
        $project->description = $request->description;
        $project->start_date = $request->start_date;
        $project->estimated_date = $request->estimated_date;
        $project->actual_date = $request->actual_date;
        $project->skills = $request->skills;
        $project->datafile_type = $request->datafile_type;
        $project->datafile_name = $request->datafile_name;
        $project->status = $request->status;
        $project->datafile_type = $request->datafile_type;
        $project->save();*/
    }

    public function show($id)
    {
        $project = Project::where('id', $id)->with('commonSkill')->first();
        //  $this->authorize('view',$project);
        return response()->json($project, 200);
    }

    public function edit($id)
    {
    }

    public function update(Request $request, $id)
    {
        $project = Project::where('id', $id)->first();
        // $this->authorize('update', $project);
        $project->update($request->all());
        return response()->json('Done Update Project', 200);
    }

    public function destroy($id)
    {
        $project = Project::where('id', $id);
        Group::where('project_id', $id)->delete();
        $project->delete();
        // $this->authorize('delete', $project);
        return response()->json('Done Destroy Project', 200);
    }

    public function addMemberToProject(Request $request)
    {
        Group::create([
            'project_id' => $request->project_id,
            'profile_id' => $request->profile_id,
            'assigned_by' => $request->user()->profile->id,
            'role' => $request->role,
        ]);
    }

    public function removeMemberFromProject(Request $request)
    {
        Group::where('project_id', $request->project_id)
            ->where('profile_id', $request->profile_id)->delete();
        return response()->json('Done Delete', 200);
    }

    public function showAllDeveloper(Request $request)
    {

       // return $request;

        $users = User::where('id', '<>', 0)->get();
        if ($request->name != "null") {
            foreach ($users as $user) {
                $users = $user->where('name', 'LIKE', '%' . $request->name . '%')->get();
            }
        }
        if ($request->grade != "null") {
            foreach ($users as $key => $user) {
                if ($user->profile->grade == $request->grade) {
                    $user_id = $user->profile->user_id;
                    $users = $user->where('id', $user_id)->get();
                } else {
                    unset($users[$key]);
                }
            }
        }

        $devs = [];
        foreach ($users as $user) {
            //decode encode for role array
            $user_roles = json_decode(json_encode($user->getRolesValueAttribute()));
            if (in_array('developer', $user_roles)) {
                // $user->profile->name = $user->name;
                $devs[] = $user;
            }
        }





        /*if ($request->skill) {
            foreach ($devs as $dev) {
                if ($dev->profile->where('grade', $request->grade)->get()) {
                    $devs = $dev;

                }
            }
        }*/

        return response()->json($devs, 200);


    }

    public function showAllProjectForUser(Request $request)
    {
        $profile_id = $request->user()->profile->id;
        $projects = Group::where('profile_id', $profile_id)->get();
        $member = collect($projects);
        $member = $member->map(function ($member) {
            return $member['project_id'];
        });
        $project = Project::whereIn('id', $member)->where('manager_id', '!=', $profile_id)->get();
        return response()->json($project, 200);
    }

    public function getAllProjectForManagerProject(Request $request)
    {
        $profile_id = $request->user()->profile->id;
        $project = Project::where('manager_id', $profile_id)->get();
        return response()->json($project, 200);
    }

    public function updateUserRoleInThisProject(Request $request, $profile_id)
    {
        $profile = Group::where('project_id', $request->project_id)
            ->where('profile_id', $profile_id);
        $profile->updat([
            'role' => $request->newRole
        ]);
        return response()->json('update Done', 200);
    }

    public function getPublicProject()
    {
        $user = Auth::user();
        $profile_id = $user->profile->id;
        $projects = Group::where('profile_id', $profile_id)->get();
        $member = collect($projects);
        $member = $member->map(function ($member) {
            return $member['project_id'];
        });
        $project = Project::whereNotIn('id', $member)->where('manager_id', '!=', $profile_id)->where('isPublic', 1)->get();
        return response()->json($project, 200);
    }

    public function sendOrderPublicProject(Request $request)
    {
        $order = new Order();

        $order->profile_id = $request->user()->profile->id;
        $order->project_id = $request->id;
        $order->save();
        return response()->json($order, 201);
    }

    public function removeOrderPublicProject(Request $request)
    {
        Order::where('project_id', $request->id)->where('profile_id', $request->user()->profile->id)->delete();
        return response()->json("Done");
    }

    public function checkOrderPublicProject(Request $request)
    {
        $order = Order::where('project_id', $request->id)->where('profile_id', $request->user()->profile->id)->first();
        if ($order)
            return "true";
        else
            return "false";
    }

    public function getPublicProjectRequests($id)
    {

        /*$jehad = Profile::find(1);
        return $jehad->user;*/


        $orders = Order::where('project_id', $id)->get();
        $users = [];
        $member = collect($orders);
        $member = $member->map(function ($member) {
            return $member['profile_id'];
        });
        // return $member;
        foreach ($member as $item) {
            $profile = Profile::where('id', $item)->first();
            $user = $profile->user;
            array_push($users, $user);
        }
        return $users;
    }

    public function acceptPublicProjectRequest(Request $request)
    {
        $profile_id = Profile::where('user_id', $request->user_id)->first();
        //return $profile_id->id;
        $order = Order::where('project_id', $request->project_id)->where('profile_id', $profile_id->id)->delete();
        //  return $order;
        if ($request->isAccepted == "true") {
            Group::create([
                'project_id' => $request->project_id,
                'profile_id' => $profile_id->id,
                'assigned_by' => $request->user()->profile->id,
            ]);
            return response()->json("Accepted");
        } else {
            return response()->json("Rejected");
        }

    }

}
