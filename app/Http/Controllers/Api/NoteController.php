<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Note;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use function PHPUnit\Framework\returnArgument;

class NoteController extends Controller
{

    public function index()
    {
        //  $this->authorize('viewAny', Note::class);
        $notes = Note::all();
        return response()->json($notes, 200);
    }

    public function create()
    {
    }

    public function store(Request $request)
    {
        //  $this->authorize('create', Note::class);
        $note = new Note([
            'noteable_id' => $request->noteable_id,
            'noteable_type' => $request->noteable_type,
            'profile_id' => $request->user()->profile->id,
            'note' => $request->note,
            'type' => $request->type
        ]);
        $note->save();
        return response()->json($note, 201);
    }

    public function show($id)
    {
        $note = Note::where('id', $id)->get();
        //  $this->authorize('view',$note);
        return response()->json($note, 200);
    }

    public function update(Request $request, $id)
    {
        $note = Note::where('id', $id)->first()->update($request->all());
        // $this->authorize('update', $note);
        return response()->json('Done Update Profile', 200);
    }

    public function getNotesForUser(Request $request)
    {
        $notes = Note::where('profile_id', $request->user()->profile->id)->get();
        return response()->json($notes, 200);
    }

    public function destroy($id)
    {
        $note = Note::where('id', $id)->delete();
        // $this->authorize('delete', $note);
        return response()->json('Done Destroy File', 200);
    }


}
