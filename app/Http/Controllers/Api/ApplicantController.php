<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Applicant;
use App\Models\Education;
use App\Models\Group;
use App\Models\Profile;
use App\Models\Project;
use App\Models\Skill;
use App\Models\Task;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use function PHPUnit\Framework\returnArgument;

class ApplicantController extends Controller
{

    public function index()
    {
        //  $this->authorize('viewAny', Applicant::class);
        $applicent = Applicant::all();
        return response()->json($applicent, 200);
    }

    public function create()
    {
    }

    public function store(Request $request)
    {
        //  $this->authorize('create', Applicant::class);
        $applicent = new Applicant([
            'task_id' => $request->task_id,
            'profile_id' => $request->user()->profile->id
        ]);
        $applicent->save();
        return response()->json($applicent, 201);
    }

    /**
     * this for show how is asked to get task
     * @param $task_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($task_id)
    {
        $applicent = Applicant::join('profiles', 'applicants.profile_id', '=', 'profiles.id')
            ->join('users', 'profiles.user_id', '=', 'users.id')
            ->select('applicants.*', 'users.name as userName', 'users.phone as userPhone', 'users.email as userEmail')
            ->where('task_id', $task_id)->get();
        //  $this->authorize('view',$applicent);


        return response()->json($applicent, 200);
    }

    /**
     * this for approve task for developer
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function approveTask(Request $request)
    {
        Applicant::where('task_id', $request->task_id)
            ->where('profile_id', $request->profile_id)
            ->update(['approved' => true]);
        return response()->json('this task is approved', 200);
    }

    /**
     * this for get all user approved them for this task
     * @param $task_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getUsersWhoApprovedThemForThisTask($task_id)
    {
        $tasks = Applicant::join('profiles', 'applicants.profile_id', '=', 'profiles.id')
            ->join('users', 'profiles.user_id', '=', 'users.id')
            ->select('applicants.*', 'users.name as userName', 'users.phone as userPhone', 'users.email as userEmail')
            ->where([['task_id', $task_id], ['approved', true], ['assigned_by', null]])->get();
        return response()->json($tasks, 200);
    }

    /**
     * this for assign task for user or more
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function assignedTaskToFreeDeveloper(Request $request)
    {
        Applicant::where([['task_id', $request->task_id], ['approved', true]])
            ->whereIn('profile_id', $request->profile_ids)
            ->update(['assigned_by' => $request->user()->profile->id]);
        return response()->json('all users have been take this task', 200);
    }

}
