<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\SkillRequest;
use App\Models\CommonSkill;
use App\Models\Skill;
use Illuminate\Http\Request;

class SkillController extends Controller
{
    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        //  $this->authorize('viewAny', Skill::class);
        $skills = Skill::all();
        return response()->json($skills, 200);
    }

    public function create()
    {
    }

    /**
     * @param SkillRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(SkillRequest $request)
    {
        // $this->authorize('create', Skill::class);
        $skills = Skill::create($request->all());
        $skills->update([
            'profile_id' => $request->user()->profile->id,
        ]);
        return response()->json('Skill Added Successfully', 201);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $skills = Skill::where('id', $id)->first();
        //  $this->authorize('view',$skills);
        return response()->json($skills, 200);
    }

    public function edit($id)
    {
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $id)
    {
        $skills = Skill::where('id', $id)->first();
        // $this->authorize('update', $skills);
        $skills->update($request->all());
        return response()->json('Skill Edited Successfully', 200);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        $skills = Skill::where('id', $id)->delete();
        // $this->authorize('delete', $skills);
        return response()->json('Skill Deleted Successfully', 200);
    }

    public function getCommonSkill(): \Illuminate\Http\JsonResponse
    {
        $common = CommonSkill::all();
        $response = [
            'Common skills' => $common,
        ];
        return response()->json($response, 200);
    }

    public function test()
    {

    }
}
