<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\ProfileRequest;
use App\Models\Group;
use App\Models\Profile;
use App\Models\User_Role;
use Illuminate\Http\Request;
use phpDocumentor\Reflection\Types\Null_;


class ProfileController extends Controller
{

    public function index()
    {

        //  $this->authorize('viewAny', Profile::class);
        $profile = Profile::all();
        return response()->json($profile, 200);
    }

    public function create(Request $request)
    {
    }

    public function store(ProfileRequest $request)
    {
        // $this->authorize('create', Profile::class);
        $profile = Profile::create($request->all());
        $profile->update([
            'user_id' => $request->user()->id,
        ]);
        $userRoleDefault = ['user_id' => $request->user()->id, 'role_id' => '3'];
        User_Role::create($userRoleDefault);

        return response()->json('Done Store Profile', 201);
    }

    public function show($id)
    {
        $profile = Profile::where('user_id',$id)->with('skill')->get()->first();

        if ($profile == NULL) {
            return response()->json('Profile is not exist', 404);
        } else {
            //  $this->authorize('view',$profile);
            return response()->json($profile, 200);
        }
    }

    public function edit($id)
    {
    }

    public function update(Request $request, $id)
    {
        $profile = Profile::where('id', $id)->first();
        //  $this->authorize('update', $profile);
        $profile->update($request->all());

        return response()->json('Done Update Profile', 200);
    }

    public function destroy($id)
    {
        $profile = Profile::where('id', $id)->delete();
        // $this->authorize('delete', $profile);

        return response()->json('Done Soft Delete Profile', 200);
    }

    /**
     *this for get all member for one project
     * @param $project_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAllMemberOfMyProject($project_id)
    {
        $getAlls = Group::where('project_id', $project_id)->get();
        $member = collect($getAlls);
        $member = $member->map(function ($member) {
            return $member['profile_id'];
        });
        $profile = Profile::join('users','profiles.user_id','=','users.id')->select('profiles.*','users.name')->whereIn('profiles.id', $member)->get();
        return response()->json($profile, 200);
    }
}
