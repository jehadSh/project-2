<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\CountryRequest;
use App\Models\Country;
use Illuminate\Http\Request;

class CountryController extends Controller
{
    public function index()
    {

        $country = Country::all();
        return response()->json($country, 200);
    }

    public function create()
    {
    }

    public function store(CountryRequest $request)
    {
        $country = new Country([
            'name' => $request->name,
            'alias' => $request->alias,
            'code' => $request->code,
            'phone_code' => $request->phone_code,

        ]);
        $country->save();
        return response()->json($country, 201);
    }

    public function show($id)
    {
        $country = Country::where('id',$id)->get();
        return response()->json($country, 200);
    }

    public function update(Request $request, $id)
    {
        Country::where('id', $id)->first()->update($request->all());
        return response()->json('Done Update Country', 200);
    }


    public function destroy($id)
    {
        Country::where('id', $id)->delete();
        return response()->json('Done Destroy Country', 200);
    }
}
