<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\EducationRequest;
use App\Models\Education;
use Illuminate\Http\Request;

class EducationController extends Controller
{

    public function index()
    {
        //  $this->authorize('viewAny', Education::class);
        $education = Education::all();
        return response()->json($education, 200);
    }
    public function create()
    {
    }

    public function store(EducationRequest $request)
    {
        //  $this->authorize('create', Education::class);
        $education = Education::create($request->all());
        $education->update([
            'profile_id' => $request->user()->profile->id,
        ]);
        return response()->json('Done Create Education', 201);
    }

    public function show($id)
    {
        $education = Education::where('id', $id)->first();
        //  $this->authorize('view',$project);
            return response()->json($education, 200);

    }

    public function edit($id)
    {
    }

    public function update(Request $request, $id)
    {
        $education = Education::where('id', $id)->first();
        // $this->authorize('update', $education);
        $education->update($request->all());
        return response()->json('Done Update Education', 200);
    }

    public function destroy($id)
    {
        $education = Education::where('id',$id)->delete();
        // $this->authorize('delete', $education);
        return response()->json('Done Delete Education', 200);
    }
}
