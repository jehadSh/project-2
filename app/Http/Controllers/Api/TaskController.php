<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Controllers\NotiController;
use App\Http\Requests\TaskRequest;
use App\Models\Group;
use App\Models\Task;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TaskController extends Controller
{
    public NotiController $notiController;

    public function __construct()
    {
        $this->notiController = new NotiController();
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        // $this->authorize('viewAny', Task::class);
        $task = Task::all();
        return response()->json($task, 200);
    }

    /**
     *
     */
    public function create()
    {
    }


    public function store(Request $request)
    {
        // $this->authorize('create', Task::class);

        $body_data = $request->body_data;

        $data = json_decode($body_data, true);

        $task = Task::create($data)->update([
            'created_by' => $request->user()->profile->id
        ]);

        return response()->json('Done Create Task', 201);

    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $task = Task::where('id', $id)->first();
        //  $this->authorize('view',$skills);
        return response()->json($task, 200);
    }


    public function edit($id)
    {
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $id)
    {
        $task = Task::where('id', $id)->first();
        // $this->authorize('update', $task);
        $task->update($request->all());
        return response()->json('Done Update Task', 200);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        $task = Task::where('id', $id)->delete();
        // $this->authorize('delete', $task);
        return response()->json('Done Delete Task', 200);
    }

    /**
     * assign task to user
     * @param Request $request
     * @param $task_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function assignTaskToUser(Request $request, $task_id)
    {
        $task = Task::where('id', $task_id)->where('project_id', $request->project_id)->get()->first();
        $task->update([
            'profile_id' => $request->profile_id,
            'status' => 'in progress'
        ]);
        //send Notification to user
        $user = User::join('profiles', 'users.id', '=', 'profiles.user_id')->select('users.*')->where('profiles.id', $request->profile_id)->get()->first();
        $this->notiController->sendNotification($user);

        return response()->json('Done assign Task to developer', 200);
    }

    public function getAllTaskForThisProject($project_id)
    {

        /*$allTask = Task::with('profile')
            ->leftJoin('users', 'profile_id', '=', 'users.id')
            ->where('project_id', $project_id)
            ->get();*/
        //SELECT Orders.OrderID, Customers.CustomerName
        //FROM Orders
        //INNER JOIN Customers ON Orders.CustomerID = Customers.CustomerID;

        $devs = [];

        $allTasks = Task::where('project_id', $project_id)->get();
        /*foreach ($allTasks as $allTask){
           // echo $allTask->profile->user_id;
           $user = User::where('id', $allTask->profile->user_id)->get();
            $devs[] = $user;
        }
       // return "sdasd";

        $response = [
            'devs' => $devs,
            'allTasks' => $allTasks
        ];*/

       // return $response;
        return response()->json($allTasks, 200);
    }

    /**
     * add task to user by self
     * @param Request $request
     * @param $task_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function addTaskForMyTask(Request $request, $task_id)
    {
        $task = Task::where('id', $task_id)->whereNull('profile_id')->get()->first();
        if ($task) {
            $profile_id = $request->user()->profile->id;
            $task->update([
                'profile_id' => $profile_id,
                'status' => $request->status,
            ]);
            return response()->json('Done add this Task', 200);
        } else {
            return response()->json('this Task is busy', 200);

        }
    }

    /**
     * @param Request $request
     * @param $project_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getTaskForUserInThisProject(Request $request, $project_id)
    {
        // show all task for this project for this Dev
        $profile_id = $request->user()->profile->id;
        $task = Task::where('project_id', $project_id)->where('profile_id', $profile_id)->get();
        return response()->json($task, 200);
    }

    /**
     * @param Request $request
     * @param $task_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function evaluationThisTask(Request $request, $task_id)
    {
        $task = Task::where('id', $task_id);
        $task->update([
            'evaluation' => $request->evaluation,
            'evaluation_date' => $request->evaluation_date,
            'evaluated_by' => $request->user()->id
        ]);
        return response()->json('evaluation Done', 200);
    }

    /**
     * @param Request $request
     * @param $project_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAllTaskForUserInThisProject(Request $request, $project_id)
    {
        $tasks = Task::where('project_id', $project_id)
            ->where('profile_id', $request->user()->profile->id)->get();
        return response()->json($tasks, 200);

    }

    /**
     * @param Request $request
     * @param $profile_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateUserRoleInThisProject(Request $request, $profile_id)
    {
        $profile = Group::where('profile_id', $profile_id)->where('project_id', $request->project_id)
            ->update(['role' => $request->newRole]);
        return response()->json($profile, 200);
    }

    /**
     * put promoted to true, to send task for free development
     * @param Request $request
     * @param $task_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function makeTaskForFreeDevelopers(Request $request, $task_id)
    {
        Task::where('id', $task_id)->update(['promoted_by' => $request->user()->profile->id]);
        return response()->json('the task promoted', 200);
    }

    /**
     * put task from free development to normal developer
     * @param Request $request
     * @param $task_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function removeTaskFromFreeDeveloper(Request $request, $task_id)
    {
        Task::where('id', $task_id)->update(['promoted_by' => null]);
        return response()->json('the task non promoted', 200);
    }

    /**
     * get task where promoted is true
     * @param Request $request
     * @param $project_id
     * @return \Illuminate\Http\JsonResponse
     */

    public function getAllTaskInFreeDevelopment( $project_id)

    {
        $tasks = Task::where('project_id', $project_id)->whereNotNull('promoted_by')->get();
        return response()->json($tasks, 200);
    }

    public function changeTaskStatus(Request $request, $id)
    {
        $tasks = Task::where('id', $request->id)->first();

        if ($request->status == "cancel") {
            $tasks->update([
                'status' => "none",
                'profile_id' => null,
            ]);
        } else {
            $tasks->update([
                'status' => $request->status,
            ]);
        }

        return response()->json($tasks, 201);
    }
}
