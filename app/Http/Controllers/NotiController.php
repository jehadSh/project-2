<?php

namespace App\Http\Controllers;

use App\Notifications\SendEmailNotification;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Notification;

class NotiController extends Controller
{
    public function sendNotification($user)
    {
        $details = [
            'greeting' => 'hi laravel Developer',
            'body' => 'this is the email body',
            'actionText' => 'show all your Task',
            'actionUrl' => 'http://127.0.0.1:8000/api/task/get_myTask/1',
            'lastLine' => 'this is the email lastline'
        ];
        Notification::send($user, new SendEmailNotification($details));
    }
}
