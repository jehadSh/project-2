<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Profile;
use App\Models\Skill;
use App\Models\CommonSkill;

class SkillsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $unpublished_skills = Skill::select('name')->where('publish_name', '=', 0)->distinct()->get();
        $published_skills = CommonSkill::select('skillName')->get();
        return view('skills/skills')->with('unpublished_skills', $unpublished_skills)->with('published_skills', $published_skills);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $skill = new Skill;
        if ($request->id != null) {
            $skill = Skill::find($request->id);
        }
        $skill->profile_id = Profile::where('user_id', auth()->user()->id)->pluck('id')->first();
        $skill->name = $request->name ?? $skill->name;
        $skill->level = $request->level ?? $skill->level;
        $skill->description = $request->description ?? $skill->description;
        $skill->last_use = $request->last_use ?? $skill->last_use;
        $skill->publish_name = Skill::where('name', $skill->name)->pluck('publish_name')->first() != null ?? 0;
        $skill->save();
 //       return redirect()->back();
   
        $unpublished_skills = Skill::select('name')->where('publish_name', '=', 0)->distinct()->get();
        $published_skills = Skill::select('name')->where('publish_name', '=', 1)->distinct()->get();
        return view('skills/skills')->with('unpublished_skills', $unpublished_skills)->with('published_skills', $published_skills);

}

    /**
     * Display the specified resource.
     *
     * @param  string  $name
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  string  $name
     * @return \Illuminate\Http\Response
     */
    public function edit($name)
    {        
        $skills = Skill::all()->where('name', $name);
        return view('skills/edit')->with("skills",$skills);
    }

    /**
     * Update the specified resource in storage.
     *s
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($name)
    {
        $skills = Skill::all()->where('name', $name);
        foreach ($skills as $skill) {
            $skill->delete();
        }
   //     return redirect()->back();
   $unpublished_skills = Skill::select('name')->where('publish_name', '=', 0)->distinct()->get();
   $published_skills = Skill::select('name')->where('publish_name', '=', 1)->distinct()->get();
   return view('skills/skills')->with('unpublished_skills', $unpublished_skills)->with('published_skills', $published_skills);
  

}

    public function publish($name)
    {
        $skills = Skill::all()->where('name', $name);
        foreach ($skills as $skill) {
            $skill->publish_name = !$skill->publish_name;
            $skill->save();
        }
        return redirect()->back();
    }

    public function evaluate(Request $request)
    {
        $skill = Skill::find($request->id);
        $skill->evaluation = $request->evaluation;
        $skill->evaluation_date = now();
        $skill->save();
        return redirect()->back();
    }
}
