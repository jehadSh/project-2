<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Role;
use App\Models\Profile;

class ProfilesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $profiles = Profile::paginate(21);
        return view('profile/profiles')
            ->with('profiles', $profiles);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $profile = new Profile;
        if ($request->id != null) {
            $profile = Profile::find($request->id);
        } else {
            $profile->user_id = $request->user_id;
        }
        $profile->status = $request->status ?? $profile->status;
        $profile->grade = $request->grade ?? $profile->grade;
        $profile->gender = $request->gender ?? $profile->gender;
        $profile->birthday = $request->birthday ?? $profile->birthday;
        $profile->city = $request->city ?? $profile->city;
        $profile->weekly_hours = $request->weekly_hours ?? $profile->weekly_hours;
        $profile->website = $request->website ?? $profile->website;
        $profile->github = $request->github ?? $profile->github;
        $profile->gitlab = $request->gitlab ?? $profile->gitlab;
        $profile->picture = $request->picture ?? $profile->picture;
        $profile->cover = $request->cover ?? $profile->cover;
        //TODO: social_media, contacts
        $profile->save();
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $profile = Profile::find($id);
        $userRoles = $profile->User->Roles;
        $userRolesIdes = [];
        foreach ($userRoles as $us) {
            array_push($userRolesIdes, $us->id);
        }
        return view('profile/show')
            ->with('profile', $profile)
           // ->with('educations', $profile->Education)
            ->with('skills', $profile->Skill)
            ->with('userRoles', $userRolesIdes)
            ->with('roles', Role::all())
            ->with('profileStatus', getProfileStatus())
            ->with('userStatus', getUserStatus());
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $profile = Profile::find($id);
        $profile->delete();
        return redirect()->back();
    }

    public function search(Request $request)
    {
        $profiles = Profile::join('users', 'users.id', '=', 'profiles.user_id')
            ->where('users.name', 'like', '%' . $request->searchValue . '%')
            ->paginate(21);
        return view('profile/profiles')
            ->with('profiles', $profiles);
    }
}
