<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Group;
use Illuminate\Support\Facades\DB;


class GroupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

       $groups =Group::select('project_id')->groupBy('project_id')->get();
        return view('groups/groups')
            ->with('groups',$groups);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $profiles=Group::select('profile_id')->where('project_id',$id)->get();
        $project_id =Group::select('project_id')->where('project_id',$id)->groupBy('project_id')->get()->first();
        return view ('groups/show')
            ->with('profiles',$profiles)
            ->with('project_id',$project_id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table('groups')->where('project_id', '=', $id)->delete();     
       $groups =Group::select('project_id')->groupBy('project_id')->get();
       return view('groups/groups')
           ->with('groups',$groups);
    }

    public function delete(Request $request, $name)
    {
        $users=DB::table('users')->where('name','=',$name)->get()->first();
        $profile= DB::table('profiles')->where('user_id','=',$users->id)->get()->first();
        $id=DB::table('projects')->where('id','=',$request->project_id)->get()->first();
        DB::table('groups')->where([['project_id','=',$id->id],['profile_id','=',$profile->id]])->delete();

        $groups =Group::select('project_id')->groupBy('project_id')->get();
        return view('groups/groups')
            ->with('groups',$groups);
       
    }

}
