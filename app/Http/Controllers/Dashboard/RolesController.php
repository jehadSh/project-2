<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Role;
use App\Models\Permission;


class RolesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('roles/roles')
            ->with('roles', Role::all())
            ->with('status', getRolesStatus());;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $role = new Role;
        if ($request->id != null) {
            $role = Role::find($request->id);
        }
        $role->name = $request->name ?? $role->name;
        $role->alias = $request->alias ?? $role->alias;
        $role->status = $request->status ?? $role->status;
        $role->save();
        if ($request->permissions != null) {
            $permissions = Permission::select('id')->whereIn('alias', $request->permissions)->get();
            $role->permissions()->sync($permissions);
        }
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $models = getModels();
        $permissions = getPermissions();
        $role = Role::find($id);
        $rolePermissions = [];

        foreach ($role->permissions as $pr) {
            array_push($rolePermissions, $pr->alias);
        }

        return view('roles/edit')
            ->with('models', $models)
            ->with('permissions', $permissions)
            ->with('role', $role)
            ->with('rolePermissions', $rolePermissions)
            ->with('status', getRolesStatus());;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $role = Role::find($id);
        if (in_array($role->alias, getMainRoles())) {
            return redirect()->back()->with('message', 'Cant Delete Primary Role');
        }
        $role->delete();
        return redirect()->back()->with('message', 'Role Deleted Primary Role');;
    }
}
