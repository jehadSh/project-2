<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Task;
use App\Models\CommonSkill;
use App\Models\Profile;
use App\Models\Project;

class TaskController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        return view('tasks/tasks')
            ->with('tasks', Task::all())
            ->with("skills",CommonSkill::all())
            ->with('profiles',Profile::all())
            ->with('projects',Project::all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $task = new Task;
        if ($request->id != null) {
            $task = Task::find($request->id);
        }
        $task->profile_id = Profile::where('user_id', auth()->user()->id)->pluck('id')->first();
        $task->created_by = Profile::where('user_id', auth()->user()->id)->pluck('id')->first();
        $task->project_id= $request->project_id ?? $task->project_id;
        $task->title = $request->title ?? $task->title;
        $task->description = $request->description ?? $task->description;
        $task->hours = $request->hours ?? $task->hours;
        $task->min_grade = $request->min_grade ?? $task->min_grade;
      //  $task->skill_Ids = $request->skill_Ids ?? $task->skill_Ids;
        $task->evaluation_date = $request->evaluation_date ?? $task->evaluation_date;
    //    $task->profile_Ids = $request->profile_Ids ?? $task->profile_Ids;

        $task->save();
     //   return redirect()->back();

     
     return view('tasks/tasks')
     ->with('tasks', Task::all())
     ->with("skills",CommonSkill::all())
     ->with('profiles',Profile::all())
     ->with('projects',Project::all());
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
        $tasks = Task::find($id);
       // return $tasks;
         return view('tasks/edit')->with("task",$tasks);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        
        $task = Task::find($id);
        $task->delete();
       // return redirect()->back();
        
     return view('tasks/tasks')
     ->with('tasks', Task::all())
     ->with("skills",CommonSkill::all())
     ->with('profiles',Profile::all())
     ->with('projects',Project::all());
    }
}
