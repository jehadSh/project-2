<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Project;
use App\Models\CommonSkill;
use App\Models\Profile;
use App\Models\Group;
use Illuminate\Support\Facades\DB;

class ProjectsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $projects = Project::paginate(20);
        $managers=DB::select('select profiles.id,users.name
                            from profiles 
                            inner join users on profiles.user_id=users.id
                            inner join role_user on users.id=role_user.user_id
                            where role_user.role_id=2');
        return view('projects/projects')
            ->with('projects', $projects)
            ->with('status',getProjectStatus())
            ->with("skills",CommonSkill::all())
            ->with('profiles',Profile::all())
            ->with('managers',$managers);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $project = new Project();
        if ($request->id != null) {
            $project = Project::find($request->id);
        }
        $project->manager_id = $request->user()->profile->id;
        $project->previous_manager_id = $request->user()->profile->id;
        $project->created_by = $request->user()->profile->id;

        $project->color = $request->color??$project->color;
        $project->title = $request->title??$project->title;
        $project->description = $request->description??$project->description;
        $project->start_date = $request->start_date??$project->start_date;
        $project->manager_id = $request->manager_id??$project->manager_id;
        $project->isPublic = $request->isPublic??$project->isPublic;
        $project->estimated_date = $request->estimated_date??$project->estimated_date;
        $project->actual_date = $request->actual_date??$project->actual_date;
        $project->status = $request->status??$project->status;
     
        $skill= implode(" ",$request->skills);
        $project->skills=$skill??$project->skills;
        $project->save();

        foreach($request->profile_Ids as $pro){
        $group=new Group();
        $group->profile_id=$pro;
        $group->project_id=$project->id;
        $group->assigned_by=$request->user()->profile->id;
        $group->save();
        }


        $projects = Project::paginate(20);
        $managers=DB::select('select profiles.id,users.name
                            from profiles 
                            inner join users on profiles.user_id=users.id
                            inner join role_user on users.id=role_user.user_id
                            where role_user.role_id=2');
        return view('projects/projects')
            ->with('projects', $projects)
            ->with('status',getProjectStatus())
            ->with("skills",CommonSkill::all())
            ->with('profiles',Profile::all())
            ->with('managers',$managers);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $project = Project::find($id);

        return view('projects/edit')
        ->with("project",$project) 
        ->with('status',getProjectStatus())
        ->with("skills",CommonSkill::all())
        ->with('profiles',Profile::all());
    
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $project = Project::find($id);
        $project->status = $request->status;
        $project->save();
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $project = Project::where('id', $id);
      //  Group::where('project_id', $id)->delete();
        $project->delete();
         return redirect()->back();    
    }
}
 