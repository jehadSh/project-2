<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Profile;
use App\Models\Education;

class EducationsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $education = new Education;
        if ($request->id != null) {
            $education = Education::find($request->id);
        }
        $education->profile_id = Profile::where('user_id', auth()->user()->id)->pluck('id')->first();
        $education->degree = $request->degree ?? $education->degree;
        $education->major = $request->major ?? $education->major;
        $education->school = $request->school ?? $education->school;
        $education->activities = $request->activities ?? $education->activities;
        $education->start_date = $request->start_date ?? $education->start_date;
        $education->end_date = $request->end_date ?? $education->end_date;
        $education->publish_degree = Education::where('degree', $education->degree)->pluck('publish_degree')->first() != null ?? 0;
        $education->publish_major = Education::where('major', $education->major)->pluck('publish_major')->first() != null ?? 0;
        $education->publish_school = Education::where('school', $education->school)->pluck('publish_school')->first() != null ?? 0;
        $education->save();
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $education = Education::find($id);
        $education->delete();
        return redirect()->back();
    }

    public function degrees()
    {
        $unpublished_degrees = Education::select('degree')->where('publish_degree', '=', 0)->distinct()->get();
        $published_degrees = Education::select('degree')->where('publish_degree', '=', 1)->distinct()->get();

        return view('educations/degrees')
            ->with('unpublished_degrees', $unpublished_degrees)
            ->with('published_degrees', $published_degrees);
    }

    public function majors()
    {
        $unpublished_majors = Education::select('major')->where('publish_major', '=', 0)->distinct()->get();
        $published_majors = Education::select('major')->where('publish_major', '=', 1)->distinct()->get();

        return view('educations/majors')
            ->with('unpublished_majors', $unpublished_majors)
            ->with('published_majors', $published_majors);
    }

    public function schools()
    {
        $unpublished_schools = Education::select('school')->where('publish_school', '=', 0)->distinct()->get();
        $published_schools = Education::select('school')->where('publish_school', '=', 1)->distinct()->get();

        return view('educations/schools')
            ->with('unpublished_schools', $unpublished_schools)
            ->with('published_schools', $published_schools);
    }

    public function publishDegree($degree)
    {
        $educations = Education::all()->where('degree', $degree);
        foreach ($educations as $education) {
            $education->publish_degree = !$education->publish_degree;
            $education->save();
        }
        return redirect()->back();
    }

    public function publishMajor($major)
    {
        $educations = Education::all()->where('major', $major);
        foreach ($educations as $education) {
            $education->publish_major = !$education->publish_major;
            $education->save();
        }
        return redirect()->back();
    }

    public function publishSchool($school)
    {
        $educations = Education::all()->where('school', $school);
        foreach ($educations as $education) {
            $education->publish_school = !$education->publish_school;
            $education->save();
        }
        return redirect()->back();
    }
}
