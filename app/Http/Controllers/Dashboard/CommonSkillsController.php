<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\CommonSkill;


class CommonSkillsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        $commonSkills = CommonSkill::all();
        return view('commonSkills/commonSkills')
        ->with('commonSkills', $commonSkills);
   
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $skill = new CommonSkill;
        $skill->skillName = $request->name;
        $skill->save();

        $commonSkills = CommonSkill::all();
        return view('commonSkills/commonSkills')
        ->with('commonSkills', $commonSkills);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        $skill = CommonSkill::find($id);
        if($skill!=null){
            $skill->skillName=$request->name;
        }
        $skill->save();
        
        $commonSkills = CommonSkill::all();
        return view('commonSkills/commonSkills')
        ->with('commonSkills', $commonSkills);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($name)
    {

        $skills = CommonSkill::all()->where('skillName', $name);
        foreach ($skills as $skill) {
            $skill->delete();
        } 
        
        $commonSkills = CommonSkill::all();
        return view('commonSkills/commonSkills')
        ->with('commonSkills', $commonSkills);

    }
}
