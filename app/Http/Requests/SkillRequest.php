<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SkillRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string',
            'last_use'=>'required|date' ,
            'level'=>'numeric|max:10' ,
        ];
    }
    public function messages()
    {
        return [
            'name.required' => 'Please enter the name skills ',
            'name.string' => 'The name skills must be string',
            'last_use.required' => 'Please enter the last_use ' ,
            'last_use.date' => 'The last_use must be date ' ,
            'level.numeric' => 'This level is number' ,
            'level.max' => 'The max level number is 10' ,
        ];
    }
}
