<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AuthRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'string|required|max:100',
            'email' => 'string|unique:users,email',
            'phone' => 'numeric|unique:users,phone',
            'password' => 'required|confirmed',
        ];
    }

    public function messages()
    {
        return [
            'name.string' => 'Please enter the name correctly' ,
            'name.required' => 'Please enter the name' ,
            'name.max' => 'The name is too big' ,
            'email.string' => 'Please enter the email correctly' ,
            'email.unique' => 'This email is already registered' ,
            'email.email' => 'Please enter an email' ,
            'phone.numeric' => 'Please enter the phone number' ,
            'phone.unique' => 'This phone is already registered' ,
            'password.required' => 'Please Enter the password' ,
            'password.confirmed' => 'Please confirm the password' ,
        ];
    }
}
