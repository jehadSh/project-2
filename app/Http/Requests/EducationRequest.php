<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EducationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'school' => 'required|string',
            'degree' => 'required|string',
            'major' => 'required|string',
            'start_date'=>'date' ,
            'end_date'=>'date|after:start_date' ,
        ];
    }

    public function messages()
    {
        return [
            'school.required' => 'Please enter the school',
            'school.string' => 'The school must be string',
            'degree.required' => 'Please enter the degree',
            'degree.string' => 'The degree must be string',
            'major.required' => 'Please enter the major',
            'major.string' => 'The major must be string',
            'start_date.date' => 'The start_date must be date' ,
            'end_date.date' => 'The end_date must be date' ,
            'end_date.after' => 'The end_date must be after start_date' ,
        ];
    }
}
