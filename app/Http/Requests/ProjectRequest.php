<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProjectRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
         //   'color'=>'required|string' ,
         //   'title'=>'required|string' ,
            'start_date'=>'date' ,
            'estimated_date'=>'date|after:start_date' ,
            'actual_date'=>'date' ,
            'datafile_name'=>'string',
            'valid_to'=>'date',

        ];
    }
    public function messages()
    {
        return [
            'color.required' => 'Please enter the color' ,
            'color.string' => 'The color must be string' ,
            'title.required' => 'Please enter the title' ,
            'title.string' => 'The title must be string' ,
            'start_date.date' => 'The start_date must be date' ,
            'estimated_date.date' => 'The estimated_date must be date' ,
            'estimated_date.after' => 'The estimated_date must be after start_date' ,
            'actual_date.date' => 'The actual_date must be date' ,
            'datafile_name.string' => 'The datafile_name must be string' ,

        ];
    }
}
