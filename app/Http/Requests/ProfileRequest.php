<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProfileRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'grade'=>'numeric|max:10' ,
            'birthday'=>'date' ,
            'city'=>'string' ,
            'weekly_hours'=>'required|numeric|max:60' ,
            'website'=>'string',
            'github'=>'string',
            'gitlab'=>'string',
            'social_media'=>'string',
            'contacts'=>'string',
        ];
    }
    public function messages()
    {
        return [
            'grade.numeric' => 'Please enter the grade as number' ,
            'grade.max' => 'The max grade number is 10' ,
            'birthday.date' => 'Please enter date of birth' ,
            'city.string' => 'Please enter the city name' ,
            'weekly_hours.required' => 'This weekly_hours is required' ,
            'weekly_hours.numeric' => 'This weekly_hours is number' ,
            'weekly_hours.max' => 'The max grade number is 60' ,
            'website.string' => 'Please enter the correct website' ,
            'github.string' => 'Please enter the correct github' ,
            'gitlab.string' => 'Please enter the correct gitlab' ,
            'social_media.string' => 'Please enter the correct social_media' ,
            'contacts.string' => 'Please enter the correct contacts' ,

        ];
    }
}
