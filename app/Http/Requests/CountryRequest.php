<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CountryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string',
            'alias' => 'required|string',
            'code' => 'required|string',
            'phone_code' => 'required|string',

        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Please enter the name Country ',
            'name.string' => 'The name Country must be string',
            'alias.required' => 'Please enter the name alias ',
            'alias.string' => 'The name alias must be string',
            'code.required' => 'Please enter the name code ',
            'code.string' => 'The name code must be string',
            'phone_code.required' => 'Please enter the name phone_code ',
            'phone_code.string' => 'The name phone_code must be string',

        ];
    }
}
