<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TaskRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
          //  'title' => 'required',
            'skills' => 'string',
            'start_at'=>'date' ,
            'actual_start'=>'date' ,
            'end_at'=>'date|after:start_at' ,
            'actual_end'=>'date|after:actual_start' ,
            'deadline'=>'date' ,
        ];
    }

    public function messages()
    {
        return [
            'title.required' => 'Please enter the title ',
            'skills.string' => 'The skills must be string',
            'start_date.date' => 'The start_date must be date' ,
            'actual_start.date' => 'The start_date must be date' ,
            'end_at.date' => 'The end_at must be date' ,
            'end_at.after' => 'The end_at must be after start_date' ,
            'actual_end.actual_end' => 'The actual_end must be date' ,
            'actual_end.after' => 'The actual_end must be after actual_start' ,
            'deadline.date' => 'The end_at must be date' ,
        ];
    }
}
