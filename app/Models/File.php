<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class File extends Model
{
    use HasFactory;
    protected $table= 'files';
    protected $fillable = ['fileable_id','fileable_type','profile_id',
        'type','url','name','created_at', 'updated_at'];

    public function Profile()
    {
        return $this->belongsTo(Profile::class);
    }
}
