<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Applicant extends Model
{
    use HasFactory;
    protected $table= 'applicants';
    protected $fillable = ['task_id','profile_id','assigned_by',
        'approved','rate','created_at', 'updated_at'];

    public function Profile()
    {
        return $this->belongsTo(Profile::class);
    }
    public function Task()
    {
        return $this->belongsTo(Task::class);
    }
}
