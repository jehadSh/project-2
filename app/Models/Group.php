<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    use HasFactory;
    protected $table= 'groups';
    protected $fillable = ['project_id','profile_id','assigned_by',
        'role','created_at', 'updated_at'];


        public function Project(){
            return $this->belongsTo(Project::class);
        }

        public function Profile(){
            return $this->belongsTo(Profile::class);
        }
}
