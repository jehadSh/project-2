<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Note extends Model
{
    use HasFactory;
    protected $table= 'notes';
    protected $fillable = ['noteable_id','noteable_type','profile_id','note',
        'type','created_at', 'updated_at'];

    public function Profile()
    {
        return $this->belongsTo(Profile::class);
    }
}
