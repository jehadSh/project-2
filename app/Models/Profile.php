<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Profile extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $table= 'profiles';
    protected $appends = ['userName'];
    protected $fillable = [
        'user_id','grade','gender',
        'birthday','country_id','city','weekly_hours','website',
        'github', 'gitlab','social_media','contacts',
        'picture', 'cover','status','created_at', 'updated_at'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
    public function getUserNameAttribute(){
        return $this->user()->pluck('name')->first();
    }
    /*public function education()
    {
        return $this->hasMany(Education::class);
    }*/
    public function skill()
    {
        return $this->hasMany(Skill::class)->where('isAccepted',1);

    }
    public function project()
    {
        return $this->hasMany(Project::class);
    }

    public function task()
    {
        return $this->hasMany(Task::class);
    }

    public function country()
    {
        return $this->belongsTo(Country::class);
    }
    public function applicant()
    {
        return $this->hasMany(Applicant::class);
    }
    public function note()
    {
        return $this->hasMany(Note::class);
    }
    /*public function comment()
    {
        return $this->hasMany(Comments::class);
    }*/
    public function file()
    {
        return $this->hasMany(File::class);
    }

}
