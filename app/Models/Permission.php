<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Permission extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $table= 'permissions';
    protected $fillable = ['name','alias','model','status',
        'created_at', 'updated_at'];

    public function roles()
    {
        return $this->belongsToMany(Role::class, 'permission_role');
    }
}
