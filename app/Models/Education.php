<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Education extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $table= 'educations';
    protected $fillable = [
        'profile_id','school','degree','major','activities',
        'start_date','end_date','publish_school',
        'publish_major','publish_degree','created_at', 'updated_at'];

    public function Profile()
    {
        return $this->belongsTo(Profile::class);
    }
}
