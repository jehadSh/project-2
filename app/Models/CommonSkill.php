<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CommonSkill extends Model
{
    use HasFactory;
    protected $table= 'common_skills';
    protected $fillable = ['skillName', 'created_at', 'updated_at'];
    protected $hidden = [ 'created_at', 'updated_at'];

}
