<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    use HasFactory;

    protected $table = 'tasks';
    protected $fillable = ['profile_id', 'created_by', 'project_id',
        'parent_id', 'title', 'description',
        'start_at', 'actual_start', 'end_at', 'actual_end',
        'hours', 'min_grade', 'evaluation', 'skills',
        'evaluation_date', 'evaluated_by', 'status', 'created_at', 'updated_at'];


    public function Profile()
    {
        return $this->belongsTo(Profile::class);
    }

    public function Project()
    {
        return $this->belongsTo(Project::class);
    }
}
