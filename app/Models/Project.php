<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Project extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $table = 'projects';
    protected $fillable = [
        'manager_id', 'previous_manager_id', 'created_by',
        'color', 'title', 'description', 'skills', 'start_date',
        'estimated_date', 'actual_date', 'datafile_type',
        'datafile_name', 'valid_to', 'isPublic', 'status', 'created_at', 'updated_at'];

    public function profile()
    {
        return $this->belongsTo(Profile::class);
    }

    public function task()
    {
        return $this->hasMany(Task::class);
    }


}
