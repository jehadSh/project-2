<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    use HasFactory;
    protected $table= 'comments';
    protected $fillable = ['commentable_id','commentable_type',
        'profile_id','reply_to','comment','created_at', 'updated_at'];

    public function Profile()
    {
        return $this->belongsTo(Profile::class);
    }
}
