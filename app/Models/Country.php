<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    use HasFactory;

    protected $table= 'countries';
    protected $fillable = ['name','alias ','code','phone_code',
        'created_at', 'updated_at'];

    public function profile()
    {
        return $this->hasMany(Profile::class);
    }
}
