<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Skill extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $table= 'skills';
    protected $fillable = [
       'profile_id','name','description','last_use','level','evaluation','skills',
        'evaluation_date','isAccepted','created_at', 'updated_at'
    ];

    public function Profile()
    {
        return $this->belongsTo(Profile::class);
    }
    public function commonSkill()
    {
        return $this->belongsToMany(CommonSkill::class,'com_skill_project');
    }
}
