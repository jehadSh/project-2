<?php

namespace App\Providers;


use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;


class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Profile' => 'App\Policies\ProfilePolicy',
        'App\Project' => 'App\Policies\ProjectPolicy',
        'App\Education' => 'App\Policies\EducationPolicy',
        'App\Skill' => 'App\Policies\SkillPolicy',
        'App\Task' => 'App\Policies\TaskPolicy',
        'App\Applicant' => 'App\Policies\ApplicantPolicy',
        'App\Comment' => 'App\Policies\CommentPolicy',
        'App\File' => 'App\Policies\FilePolicy',
        'App\Group' => 'App\Policies\GroupPolicy',
        'App\Note' => 'App\Policies\NotePolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();
    }
}
