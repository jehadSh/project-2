<?php

use App\Http\Controllers\TestController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Dashboard\RolesController;
use App\Http\Controllers\Dashboard\AccountsController;
use App\Http\Controllers\Dashboard\ProfilesController;
use App\Http\Controllers\Dashboard\EducationsController;
use App\Http\Controllers\Dashboard\SkillsController;
use App\Http\Controllers\Dashboard\ProjectsController;
use App\Http\Controllers\Dashboard\TaskController;
use App\Http\Controllers\Dashboard\GroupController;
use App\Http\Controllers\Dashboard\CommonSkillsController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return auth()->check() ?
        redirect('profiles') :
        redirect('login');
});

Route::middleware(['auth:sanctum', 'verified'])->get('/profiles', function () {
    return redirect('profiles');
})->name('profiles');

//Roles
Route::group(['prefix' => 'roles', 'middleware' => 'auth:sanctum'], function () {
    Route::get('', [RolesController::class, 'index'])->name('roles');
    Route::post('', [RolesController::class, 'store'])->name('roles.store');
    Route::get('/edit/{id}', [RolesController::class, 'edit'])->name('roles.edit');
    Route::get('/destroy/{id}', [RolesController::class, 'destroy'])->name('roles.destroy');
});

//Accounts
Route::group(['prefix' => 'accounts', 'middleware' => 'auth:sanctum'], function () {
    Route::post('/update/{id}', [AccountsController::class, 'update'])->name('accounts.update');
});

//Profiles
Route::group(['prefix' => 'profiles', 'middleware' => 'auth:sanctum'], function () {
    Route::get('', [ProfilesController::class, 'index'])->name('profiles');
    Route::post('', [ProfilesController::class, 'store'])->name('profiles.store');
    Route::get('/search', [ProfilesController::class, 'search'])->name('profiles.search');
    Route::get('/{id}', [ProfilesController::class, 'show'])->name('profiles.show');
});

//Educations
Route::group(['prefix' => 'educations', 'middleware' => 'auth:sanctum'], function () {
    Route::get('/degrees', [EducationsController::class, 'degrees'])->name('educations.degrees');
    Route::get('/majors', [EducationsController::class, 'majors'])->name('educations.majors');
    Route::get('/schools', [EducationsController::class, 'schools'])->name('educations.schools');
    Route::post('/store', [EducationsController::class, 'store'])->name('educations.store');
    Route::get('/degree/publish/{name}', [EducationsController::class, 'publishDegree'])->name('educations.degree.publish');
    Route::get('/major/publish/{name}', [EducationsController::class, 'publishMajor'])->name('educations.major.publish');
    Route::get('/school/publish/{name}', [EducationsController::class, 'publishSchool'])->name('educations.school.publish');
    Route::get('/destroy/{id}', [EducationsController::class, 'destroy'])->name('educations.destroy');
});

//Skills
Route::group(['prefix' => 'skills', 'middleware' => 'auth:sanctum'], function () {
    Route::get('', [SkillsController::class, 'index'])->name('skills');
    Route::post('/store', [SkillsController::class, 'store'])->name('skills.store');
    Route::get('/publish/{name}', [SkillsController::class, 'publish'])->name('skills.publish');
    Route::post('/evaluate', [SkillsController::class, 'evaluate'])->name('skills.evaluate');
    Route::get('/destroy/{name}', [SkillsController::class, 'destroy'])->name('skills.destroy');
    Route::get('/edit/{name}', [SkillsController::class, 'edit'])->name('skills.edit');
    Route::post('/update/{id}', [SkillsController::class, 'update'])->name('skills.update');

});


//Projects
Route::group(['prefix' => 'projects', 'middleware' => 'auth:sanctum'], function () {
    Route::get('', [ProjectsController::class, 'index'])->name('projects');
    Route::post('/store', [ProjectsController::class, 'store'])->name('projects.store');
    Route::post('/{id}', [ProjectsController::class, 'update'])->name('projects.update');
    Route::get('/edit/{id}', [ProjectsController::class, 'edit'])->name('projects.edit');
    Route::get('/destroy/{id}', [ProjectsController::class, 'destroy'])->name('projects.destroy');
    Route::post('/update/{id}', [ProjectsController::class, 'update'])->name('projects.update');


});


//Test
Route::get('send',[TestController::class,'sendNotification']);



//Tasks
Route::group(['prefix' => 'tasks', 'middleware' => 'auth:sanctum'], function () {
    Route::get('', [TaskController::class, 'index'])->name('tasks');
    Route::post('/store', [TaskController::class, 'store'])->name('tasks.store');
    Route::get('/destroy/{id}', [TaskController::class, 'destroy'])->name('tasks.destroy');
    Route::get('/edit/{id}', [TaskController::class, 'edit'])->name('tasks.edit');
    Route::post('/update/{id}', [TaskController::class, 'update'])->name('tasks.update');

});

//Groups
Route::group(['prefix' => 'groups', 'middleware' => 'auth:sanctum'], function () {
    Route::get('', [GroupController::class, 'index'])->name('groups');
    Route::post('/store', [GroupController::class, 'store'])->name('groups.store');
    Route::get('/destroy/{id}', [GroupController::class, 'destroy'])->name('groups.destroy');
    Route::post('/delete/{name}', [GroupController::class, 'delete'])->name('groups.delete');
    Route::post('/update/{id}', [GroupController::class, 'update'])->name('groups.update');
    Route::get('/{id}', [GroupController::class, 'show'])->name('groups.show');

});


//CommonSkills
Route::group(['prefix' => 'commonSkills', 'middleware' => 'auth:sanctum'], function () {
    Route::get('', [CommonSkillsController::class, 'index'])->name('commonSkills');
    Route::post('/store', [CommonSkillsController::class, 'store'])->name('commonSkills.store');
    Route::get('/destroy/{name}', [CommonSkillsController::class, 'destroy'])->name('commonSkills.destroy');
    Route::post('/update/{id}', [CommonSkillsController::class, 'update'])->name('commonSkills.update');

});