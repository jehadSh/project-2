<?php

use App\Http\Controllers\Api\AuthController;
use App\Http\Controllers\Api\CommentController;
use App\Http\Controllers\Api\CountryController;
use App\Http\Controllers\Api\EducationController;
use App\Http\Controllers\Api\ProfileController;
use App\Http\Controllers\Api\ProjectController;
use App\Http\Controllers\Api\SkillController;
use App\Http\Controllers\Api\TaskController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\ApplicantController;
use App\Http\Controllers\Api\FileController;
use App\Http\Controllers\Api\NoteController;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post("/login", [AuthController::class, 'login']);
Route::post("/register", [AuthController::class, 'register']);

Route::group(['middleware' => 'auth:sanctum'], function () {
    Route::post("/logout", [AuthController::class, 'logout']);
    Route::get("isToken", [AuthController::class, 'isToken']);
});
//CRUD For All Table
Route::group(['middleware' => 'auth:sanctum'], function () {
    Route::resource('profiles', ProfileController::class);
    Route::resource('educations', EducationController::class);
    Route::resource('skills', SkillController::class);
    Route::resource('projects', ProjectController::class);
    Route::resource('tasks', TaskController::class);
    Route::resource('applicants',ApplicantController::class);
    Route::resource('countries',CountryController::class);
    Route::resource('files',FileController::class);
    Route::resource('notes',NoteController::class);
    Route::resource('comments',CommentController::class);
});

Route::group(['prefix' => 'project', 'middleware' => 'auth:sanctum'], function () {
    Route::post('add-member', [ProjectController::class, 'addMemberToProject']);
    Route::get('get-all-member/{project_id}', [ProfileController::class, 'getAllMemberOfMyProject']);
    Route::delete('remove-member', [ProjectController::class, 'removeMemberFromProject']);
    Route::post('show-all-user', [ProjectController::class, 'showAllDeveloper']);
    Route::get('show-all-project', [ProjectController::class, 'showAllProjectForUser']);
    Route::get('get-all-project-for-manager-project', [ProjectController::class, 'getAllProjectForManagerProject']);
    Route::get('getPublicProject', [ProjectController::class, 'getPublicProject']);
    Route::post('sendOrderPublicProject', [ProjectController::class, 'sendOrderPublicProject']);
    Route::delete('removeOrderPublicProject', [ProjectController::class, 'removeOrderPublicProject']);
    Route::post('checkOrderPublicProject', [ProjectController::class, 'checkOrderPublicProject']);
    Route::get('getPublicProjectRequests/{id}', [ProjectController::class, 'getPublicProjectRequests']);
    Route::post('acceptPublicProjectRequest', [ProjectController::class, 'acceptPublicProjectRequest']);
});

Route::group(['prefix' => 'skill', 'middleware' => 'auth:sanctum'], function () {
    Route::get('getCommonSkill', [SkillController::class, 'getCommonSkill']);
    Route::get('test', [SkillController::class, 'test']);
});


Route::group(['prefix'=>'task','middleware' => 'auth:sanctum'],function (){
    Route::get('assign-task-to-user/{task_id}',[TaskController::class,'assignTaskToUser']);
    Route::get('get-all-task-for-this-project/{project_id}',[TaskController::class,'getAllTaskForThisProject']);
    Route::put('add-task-for-my-task/{task_id}',[TaskController::class,'addTaskForMyTask']);
    Route::get('get-myTask/{project_id}',[TaskController::class,'getTaskForUserInThisProject']);
    Route::put('evaluation-task/{task_id}',[TaskController::class,'evaluationThisTask']);
    Route::get('get-tasks-in-project/{project_id}',[TaskController::class,'getAllTaskForUserInThisProject']);
    Route::put('update-member-role-in-project/{profile_id}',[TaskController::class,'updateUserRoleInThisProject']);
    Route::get('task-promoted/{task_id}',[TaskController::class,'makeTaskForFreeDevelopers']);
    Route::get('task-non-promoted/{task_id}',[TaskController::class,'removeTaskFromFreeDeveloper']);
    Route::get('get-tasks-promoted/{project_id}',[TaskController::class,'getAllTaskInFreeDevelopment']);
    Route::post('change-task-status/{id}',[TaskController::class,'changeTaskStatus']);

});

/*Route::group(['prefix'=>'applicant','middleware' => 'auth:sanctum'],function (){
    Route::put('approve-task',[ApplicantController::class,'approveTask']);
    Route::get('get-users-who-approved-them-for-this-task/{task_id}',[ApplicantController::class,'getUsersWhoApprovedThemForThisTask']);
    Route::put('assign-task-for-free-developer',[ApplicantController::class,'assignedTaskToFreeDeveloper']);
});*/

/*Route::group(['prefix'=>'file','middleware' => 'auth:sanctum'],function (){
    Route::get('get-files-for-user',[FileController::class,'getFilesForUser']);
});

Route::group(['prefix'=>'note','middleware' => 'auth:sanctum'],function (){
    Route::get('get-notes-for-user',[NoteController::class,'getNotesForUser']);
});

Route::group(['prefix'=>'comment','middleware' => 'auth:sanctum'],function (){
    Route::get('get-comments-for-user',[CommentController::class,'getCommentsForUser']);
});*/


